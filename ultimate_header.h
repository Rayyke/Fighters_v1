/*
ultimate_header.h

Bojovnici 1.1 BETA
==================

Peter Cizmar 4.4.2016
*/

#ifndef ULTIMATE_HEADER_H // formalitka proti opakovanemu definovaniu headeru
#define ULTIMATE_HEADER_H

#define _CRT_SECURE_NO_WARNINGS // kvoli visualku

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h> // srand() a timer
#include <conio.h> // getch() - to iste ako getchar() ale bez ECHO

#pragma comment(lib, "winmm") // mciSendString() - hudba + vysunutie mechaniky (character set musi byt multibyte a nie unicode!!! .. inak warning a nefunkcnost)

#define MORE_LONGER_PAUSE()     Sleep(6000)
#define LONGER_PAUSE()      	Sleep(3000)
#define LONG_PAUSE()      		Sleep(2500)
#define PAUSE()      			Sleep(1500)
#define PAUSE_DOTS()			Sleep(1080)
#define SHORT_PAUSE()      		Sleep(550)
#define FLASH()  				Sleep(65)
#define CLEAR_SCREEN()       	system("cls")
#define EXIT 					{CLEAR_SCREEN(); gotoxy(49, 14); exit_message(); LONGER_PAUSE(); CLEAR_SCREEN(); SHORT_PAUSE(); exit(0);} // makro pri ukonceni
#define MAX_NAME_LENGTH 		33
#define MAX_UNITS_TOTAL 		10
#define ASCII_NUM_VAL 			48 // 48 - pozicia nuly v ASCII
#define NUM_OF_ACHIEVMENTS		15
#define ERR_MSG 				printf("\n\n%s\a", (setup.language == 0) ? "Neplatny vstup, zadaj znovu!" : "Wrong input, enter the value again!")//sprava pri chybe
#define ERR_MSG_OR_DEAD 		printf("\n%s\n\a", (setup.language == 0) ? "Neplatny vstup, alebo bojovnik je uz mrtvy!" : "Wrong input, or the unit is already dead!")//sprava pri chybe alebo mrtvom bojovnikovi
#define DATA_SAVED_SUCCESSFULLY {gotoxy(27, 14); printf("%s", (setup.language == 0) ? "Data su uspesne ulozene v game_summary.txt!" : "Data are successfully saved in game_summary.txt!"); LONGER_PAUSE(); SHORT_PAUSE(); CLEAR_SCREEN(); SHORT_PAUSE();}

typedef
	struct overview{
		// overall
		double game_duration;
		double fight_durations_total;
		int number_of_fights;
		double average_fight_duration;
		char language[MAX_NAME_LENGTH];
		char allowed_bonuses[MAX_NAME_LENGTH];
		char winner[MAX_NAME_LENGTH];
		char winner_of_coin[MAX_NAME_LENGTH]; // meno
		char side_of_coin[MAX_NAME_LENGTH]; 
	   	int souls_spent;
	   	int summoned_demons;
	   	int swap_used;
	   	int items_purchased;
	   	double average_attack_power_generated;
	   	double average_defence_power_generated;
	   	double average_attack_defence_rate_generated;
	
	   	// hrac 1
	   	int units_left_p1;
	   	char summoned_demon_p1[MAX_NAME_LENGTH];
	   	char swap_used_p1[MAX_NAME_LENGTH];
	   	int souls_spent_p1;
	   	int items_purchased_p1;
	   	int hits_p1;
	   	int non_hits_p1;
	   	double hit_rate_p1;
	   	double average_attack_power_p1_generated; // priemer utocnej sily jednotiek
	    double average_defence_power_p1_generated;
	   	double average_attack_defence_rate_p1_generated;
	   	
	   	// hrac 2
	   	int units_left_p2;
	   	char summoned_demon_p2[MAX_NAME_LENGTH];
	   	char swap_used_p2[MAX_NAME_LENGTH];
	   	int souls_spent_p2;
	   	int items_purchased_p2;
	   	int hits_p2;
	   	int non_hits_p2;
	   	double hit_rate_p2;
	   	double average_attack_power_p2_generated;
	   	double average_defence_power_p2_generated;
	   	double average_attack_defence_rate_p2_generated;
	}
OVERVIEW;
	
typedef
	struct setup_settings{
	   	int first_round; // 0 znaci prve kolo hry
	   	int language; // zobrazovaci jazyk
	   	int who_attacks; // kto utoci prave?
	   	int abilities; // ON / OFF
	   	int abilities_allow[2]; // [0/1] hrac 1 a 2: hodnota 0 - pouzit special abilities tentokrat, 1 - nepouzit...dovod rekurzie
	   	int active_unit[2]; // [0/1] hrac 1 a 2: aktivna jednotka do boja pre daneho hraca 
		int music_allowed; // 0 - disabled, 1 - enabled	
		int cd_rom_eject_allowed; // 0 - disabled, 1 - enabled	
		int presentation_mode; // 0 - disabled, 1 - enabled	
	   	time_t timer_game[2]; // 0 - start, 1 - end
	   	time_t timer_fight[2]; // 0 - start, 1 - end
	   	OVERVIEW all_stats;
	}
SETUP_SETTINGS;

typedef
	struct achievments{
		int win_a_coin; // musi byt 1
		int demon_summoner; // musi byt 1
		int killing_spree[2]; // killing_spree[0] musi byt 2 pre "double kill" atd...vyhry bitiek bez porazky, killing_spree[1] - pomocne pocitadlo
		int alchemist; // 4x hociaky elixir, musi byt 4+
		int soldier; // 4x magicka puska, musi byt 4+
		int sniper; // doraz 2 nepriatelov magickou puskou, musi byt 2+
		int mass_destruction; // doraz 2 nepriatelov burkou dusi, musi byt 2+
		int legendary_possession; // musi byt 1+
		int heart_warming; //musi byt 1+, ziskaj jedno srdce
		int last_man_standing; // vyhraj iba s jednym vojakom, musi byt 1
		int brothers_in_war; // pouzi swap, musi byt 1
		int lucky_strike[2]; // zasiahni 5x po sebe, lucky_strike[0] - ak 1, tak unlock, lucky_strike[1] - pomocne pocitadlo (ak 5, zapis 1 do prvej bunky)
		int bad_luck_brian[2]; // NEzasiahni 5x po sebe, bad_luck_brian[0] - ak 1, tak unlock, bad_luck_brian[1] - pomocne pocitadlo (ak 5, zapis 1 do prvej bunky)
		int easter_egg; // "meno: Souleater" (Hint: Skus ine tematicke meno), unlock ak 1
		int no_bonus_baby; // 0 pre unlock
	}
ACHIEVMENTS;

typedef 
	struct unit_stats{ // malym je nazov struktury, velkym otvoreny pristup...t.j. lubovolny pocet pristupov k strukture (moj pripad je cez pole)
		char name[MAX_NAME_LENGTH];
		int health_pool;
		int attack;
		int defence;
	}
UNIT_STATS;

typedef 
	struct player_info{
		char name[MAX_NAME_LENGTH];
		int souls;
		int demon_available;
		int swap_available;
		int alive_units;
		int total_achievment_points;
		ACHIEVMENTS achievments;
		UNIT_STATS army[MAX_UNITS_TOTAL/2];	//pole jednotiek kazdeho hraca
	}
PLAYER_INFO;

//extern - funkcia plati len pre vsetky subory s ktorymi zdiela "ultimate_header.h"

//cizmar.peter.z3.c
extern void language_selection(void); // prvotne vetvenie
extern void help(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup); 
extern void load(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup); // zakladna inicializacia
extern void units_generator(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup); // generujem units(do 1 pola struktury)
extern void units_sorting(UNIT_STATS units_all[], PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup); // hraci si vyberu units
extern void play(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup);
extern void fight(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup);
extern void coin_of_dark_pact(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup);
extern void save_data(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup);

//setup.c
extern void gotoxy(int x,int y);
extern void adjust_window_size(void);
extern void hide_cursor(void);
extern void basic_setup(SETUP_SETTINGS setup); // zakladne nastavenia hry, daju sa meni uzivatelom na zaciatku hry

//special_abilities.c
extern void player_1_special_abilities(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup);
extern void player_2_special_abilities(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup);

//check_functions.c
extern void check_winner(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup);
extern void check_achievments(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup);

//tables.c
extern void show_player_1_units_status(PLAYER_INFO player_1, SETUP_SETTINGS setup);
extern void show_player_2_units_status(PLAYER_INFO player_2, SETUP_SETTINGS setup);
extern void show_player_1_units_selection(PLAYER_INFO player_1, SETUP_SETTINGS setup);
extern void show_player_2_units_selection(PLAYER_INFO player_2, SETUP_SETTINGS setup);
extern void show_units_fight_status(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup);

//illustrations.c
extern void intro_text(SETUP_SETTINGS setup);
extern void outro_text(SETUP_SETTINGS setup);
extern void grave(void);
extern void demon_summoning(void);
extern void potion_of_power(void);
extern void potion_of_healing(void);
extern void magic_rifle(void);
extern void rain_of_souls(void);
extern void shamanism(void);
extern void war_heart(void);
extern void heart_of_courage(void);
extern void willbreaker(void);
extern void titanskin(void);
extern void swords(void);
extern void exit_message(void);

#endif