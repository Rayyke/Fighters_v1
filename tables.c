/*
tables.c

Bojovnici 1.1 BETA
==================

Peter Cizmar 5.4.2016
*/

#include "ultimate_header.h"

extern void show_player_1_units_selection(PLAYER_INFO player_1, SETUP_SETTINGS setup){
	register int j;
	
	printf("%s", (setup.language == 0) ? "     Meno\t\tZivoty\tUtok\tObrana\n" : "     Name\t\tHP\tAttack\tDefence\n");
 	printf("%s", (setup.language == 0) ? "==============================================\n" : "===============================================\n");
 		
 	for(j = 0; j < MAX_UNITS_TOTAL/2; j++){
 		if(player_1.army[j].health_pool != 0)
 			printf("[%i]  %s\t\t%i\t%i\t%i\n", j, player_1.army[j].name, player_1.army[j].health_pool, player_1.army[j].attack, player_1.army[j].defence);
	}	
}

extern void show_player_2_units_selection(PLAYER_INFO player_2, SETUP_SETTINGS setup){
	register int j;
	
	printf("%s", (setup.language == 0) ? "     Meno\t\tZivoty\tUtok\tObrana\n" : "     Name\t\tHP\tAttack\tDefence\n");
 	printf("%s", (setup.language == 0) ? "==============================================\n" : "===============================================\n");
	
	for(j = 0; j < MAX_UNITS_TOTAL/2; j++){
		if(player_2.army[j].health_pool != 0)
				printf("[%i]  %s\t\t%i\t%i\t%i\n", j, player_2.army[j].name, player_2.army[j].health_pool, player_2.army[j].attack, player_2.army[j].defence);	
	}
}

extern void show_player_1_units_status(PLAYER_INFO player_1, SETUP_SETTINGS setup){
	register int j;
	
	printf("%s%s\n", player_1.name, (setup.language == 0) ? ", mas tychto bojovnikov!" : ", you have got these units!");

	printf("%s\n", (setup.language == 0) ? "==============================================" : "=================================================");		
	for(j = 0; j < MAX_UNITS_TOTAL/2; j++){
		if(player_1.army[j].health_pool != 0){
			if(setup.language == 0){
				if(strlen(player_1.army[j].name) < 8) //spravne zarovnanie
					printf("%s\t\t(HP: %2i, Utok: %2i, Obrana: %2i)\n", player_1.army[j].name, player_1.army[j].health_pool, player_1.army[j].attack, player_1.army[j].defence);
				else
					printf("%s\t(HP: %2i, Utok: %2i, Obrana: %2i)\n", player_1.army[j].name, player_1.army[j].health_pool, player_1.army[j].attack, player_1.army[j].defence);	
			}
			else{
				if(strlen(player_1.army[j].name) < 8) //spravne zarovnanie
					printf("%s\t\t(HP: %2i, Attack: %2i, Defence: %2i)\n", player_1.army[j].name, player_1.army[j].health_pool, player_1.army[j].attack, player_1.army[j].defence);
				else
					printf("%s\t(HP: %2i, Attack: %2i, Defence: %2i)\n", player_1.army[j].name, player_1.army[j].health_pool, player_1.army[j].attack, player_1.army[j].defence);
			}
		}	
	}	

	printf("%s\n", (setup.language == 0) ? "==============================================" : "=================================================");			
}

extern void show_player_2_units_status(PLAYER_INFO player_2, SETUP_SETTINGS setup){
	register int j;
	
	printf("%s%s\n", player_2.name, (setup.language == 0) ? ", mas tychto bojovnikov!" : ", you have got these units!");

	printf("%s\n", (setup.language == 0) ? "==============================================" : "=================================================");		
	for(j = 0; j < MAX_UNITS_TOTAL/2; j++){
		if(player_2.army[j].health_pool != 0){
			if(setup.language == 0){
				if(strlen(player_2.army[j].name) < 8) //spravne zarovnanie
					printf("%s\t\t(HP: %2i, Utok: %2i, Obrana: %2i)\n", player_2.army[j].name, player_2.army[j].health_pool, player_2.army[j].attack, player_2.army[j].defence);
				else
					printf("%s\t(HP: %2i, Utok: %2i, Obrana: %2i)\n", player_2.army[j].name, player_2.army[j].health_pool, player_2.army[j].attack, player_2.army[j].defence);	
			}
			else{
				if(strlen(player_2.army[j].name) < 8) //spravne zarovnanie
					printf("%s\t\t(HP: %2i, Attack: %2i, Defence: %2i)\n", player_2.army[j].name, player_2.army[j].health_pool, player_2.army[j].attack, player_2.army[j].defence);
				else
					printf("%s\t(HP: %2i, Attack: %2i, Defence: %2i)\n", player_2.army[j].name, player_2.army[j].health_pool, player_2.army[j].attack, player_2.army[j].defence);
			}
		}	
	}	

	printf("%s\n", (setup.language == 0) ? "==============================================" : "=================================================");		
}

extern void show_units_fight_status(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup){
	
	printf("%s\n", player_1.name);

	printf("%s\n", (setup.language == 0) ? "==============================================" : "=================================================");	

	if(setup.language == 0){
		if(strlen(player_1.army[setup.active_unit[0]].name) < 8) //spravne zarovnanie
			printf("%s\t\t(HP: %2i, Utok: %2i, Obrana: %2i)\n", player_1.army[setup.active_unit[0]].name, player_1.army[setup.active_unit[0]].health_pool, 
															 player_1.army[setup.active_unit[0]].attack, player_1.army[setup.active_unit[0]].defence);
		else
			printf("%s\t(HP: %2i, Utok: %2i, Obrana: %2i)\n", player_1.army[setup.active_unit[0]].name, player_1.army[setup.active_unit[0]].health_pool, 
														   player_1.army[setup.active_unit[0]].attack, player_1.army[setup.active_unit[0]].defence);	
	}
	else{
		if(strlen(player_1.army[setup.active_unit[0]].name) < 8) //spravne zarovnanie
			printf("%s\t\t(HP: %2i, Attack: %2i, Defence: %2i)\n", player_1.army[setup.active_unit[0]].name, player_1.army[setup.active_unit[0]].health_pool, 
																player_1.army[setup.active_unit[0]].attack, player_1.army[setup.active_unit[0]].defence);
		else
			printf("%s\t(HP: %2i, Attack: %2i, Defence: %2i)\n", player_1.army[setup.active_unit[0]].name, player_1.army[setup.active_unit[0]].health_pool, 
															  player_1.army[setup.active_unit[0]].attack, player_1.army[setup.active_unit[0]].defence);
	}
		
	printf("%s\n", (setup.language == 0) ? "==============================================" : "=================================================");
	
	printf("\n\n");
	
	printf("%s\n", player_2.name);

	printf("%s\n", (setup.language == 0) ? "==============================================" : "=================================================");	

	if(setup.language == 0){
		if(strlen(player_2.army[setup.active_unit[1]].name) < 8) //spravne zarovnanie
			printf("%s\t\t(HP: %2i, Utok: %2i, Obrana: %2i)\n", player_2.army[setup.active_unit[1]].name, player_2.army[setup.active_unit[1]].health_pool, 
															 player_2.army[setup.active_unit[1]].attack, player_2.army[setup.active_unit[1]].defence);
		else
			printf("%s\t(HP: %2i, Utok: %2i, Obrana: %2i)\n", player_2.army[setup.active_unit[1]].name, player_2.army[setup.active_unit[1]].health_pool, 
														   player_2.army[setup.active_unit[1]].attack, player_2.army[setup.active_unit[1]].defence);	
	}
	else{
		if(strlen(player_2.army[setup.active_unit[1]].name) < 8) //spravne zarovnanie
			printf("%s\t\t(HP: %2i, Attack: %2i, Defence: %2i)\n", player_2.army[setup.active_unit[1]].name, player_2.army[setup.active_unit[1]].health_pool, 
																player_2.army[setup.active_unit[1]].attack, player_2.army[setup.active_unit[1]].defence);
		else
			printf("%s\t(HP: %2i, Attack: %2i, Defence: %2i)\n", player_2.army[setup.active_unit[1]].name, player_2.army[setup.active_unit[1]].health_pool, 
															  player_2.army[setup.active_unit[1]].attack, player_2.army[setup.active_unit[1]].defence);
	}
		
	printf("%s\n", (setup.language == 0) ? "==============================================" : "=================================================");	
}
