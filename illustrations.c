/*
illustrations.c

Bojovnici 1.1 BETA
==================

Peter Cizmar 9.4.2016
*/

#include "ultimate_header.h"

extern void intro_text(SETUP_SETTINGS setup){
	int x = 35, y = 15;
	
 	if(setup.language == 0){
		gotoxy(x,y++); printf("*******************************\n");
		gotoxy(x,y++); printf("*                             *\n");
		gotoxy(x,y++); printf("*    Vitaj v hre Bojovnici    *\n");
		gotoxy(x,y++); printf("*                             *\n");
		gotoxy(x,y++); printf("*******************************\n");
		gotoxy(x,y++); printf("              **			  	  \n");
		gotoxy(x,y++); printf("              **			   	  \n");
		gotoxy(x,y++); printf("              **      		  \n");
		gotoxy(x,y++); printf("              **			   	  \n");
		gotoxy(x,y++); printf("              **			      \n");
		gotoxy(x,y++); printf("              **   			  \n");
		gotoxy(x,y++); printf("              **			      \n");
		gotoxy(x,y++); printf("              **			      \n");
		gotoxy(x,y++); printf("              **  		      \n");
		gotoxy(x,y++); printf("          **********             ");
	}	
  else{
  		gotoxy(x,y++); printf("*******************************\n");
		gotoxy(x,y++); printf("*                             *\n");
		gotoxy(x,y++); printf("*   Welcome to the Warriors   *\n");
		gotoxy(x,y++); printf("*                             *\n");
		gotoxy(x,y++); printf("*******************************\n");
		gotoxy(x,y++); printf("              **			  	  \n");
		gotoxy(x,y++); printf("              **			   	  \n");
		gotoxy(x,y++); printf("              **      		  \n");
		gotoxy(x,y++); printf("              **			   	  \n");
		gotoxy(x,y++); printf("              **			      \n");
		gotoxy(x,y++); printf("              **   			  \n");
		gotoxy(x,y++); printf("              **			      \n");
		gotoxy(x,y++); printf("              **			      \n");
		gotoxy(x,y++); printf("              **  		      \n");
		gotoxy(x,y++); printf("          **********             ");
  }
}

extern void outro_text(SETUP_SETTINGS setup){
	int x = 35, y = 15; 
	
  	if(setup.language == 0){
	  	gotoxy(x,y++); printf("*******************************\n");
		gotoxy(x,y++); printf("*                             *\n");
		gotoxy(x,y++); printf("*         Koniec hry          *\n");
		gotoxy(x,y++); printf("*                             *\n");
		gotoxy(x,y++); printf("*******************************\n");
		gotoxy(x,y++); printf("              **			  	  \n");
		gotoxy(x,y++); printf("              **			   	  \n");
		gotoxy(x,y++); printf("              **      		  \n");
		gotoxy(x,y++); printf("              **			   	  \n");
		gotoxy(x,y++); printf("              **			      \n");
		gotoxy(x,y++); printf("              **   			  \n");
		gotoxy(x,y++); printf("              **			      \n");
		gotoxy(x,y++); printf("              **			      \n");
		gotoxy(x,y++); printf("              **  		      \n");
		gotoxy(x,y++); printf("          **********             ");
  	}	
  	else{
  		gotoxy(x,y++); printf("*******************************\n");
		gotoxy(x,y++); printf("*                             *\n");
		gotoxy(x,y++); printf("*          Game over          *\n");
		gotoxy(x,y++); printf("*                             *\n");
		gotoxy(x,y++); printf("*******************************\n");
		gotoxy(x,y++); printf("              **			  	  \n");
		gotoxy(x,y++); printf("              **			   	  \n");
		gotoxy(x,y++); printf("              **      		  \n");
		gotoxy(x,y++); printf("              **			   	  \n");
		gotoxy(x,y++); printf("              **			      \n");
		gotoxy(x,y++); printf("              **   			  \n");
		gotoxy(x,y++); printf("              **			      \n");
		gotoxy(x,y++); printf("              **			      \n");
		gotoxy(x,y++); printf("              **  		      \n");
		gotoxy(x,y++); printf("          **********           ");
  	}	
}

extern void grave(void){
	int x = 32, y = 19; 

	system("COLOR 08");
	
	gotoxy(x,y++); printf("            *********              \n");
    gotoxy(x,y++); printf("         ***************           \n");
	gotoxy(x,y++); printf("        ***           ***          \n");
	gotoxy(x,y++); printf("       **               **         \n");
	gotoxy(x,y++); printf("       **      RIP      **         \n");
	gotoxy(x,y++); printf("       **               ** 		  \n");
	gotoxy(x,y++); printf("       **               **         \n");
	gotoxy(x,y++); printf("       **               **         \n");
	gotoxy(x,y++); printf("       **               ** 		  \n");
	gotoxy(x,y++); printf(".      **   ...      .  **   ..    \n"); 
	gotoxy(x,y++); printf("***********************************");    
}

extern void demon_summoning(void){
	int x = 38, y = 10; 
	register int i;
	
	CLEAR_SCREEN();
	
	for(i = 0; i < 21; i++){
		if((i % 2) == 0)
			system("COLOR 04");
		else
			system("COLOR 0C");
		
		gotoxy(x,y++); printf("       *    *    *       \n");
		gotoxy(x,y++); printf("   *                 *   \n");
		gotoxy(x,y++); printf("    *  *         *  *    \n");
		gotoxy(x,y++); printf(" *   *     * *     *   * \n");
		gotoxy(x,y++); printf("      *   *   *   *      \n");
		gotoxy(x,y++); printf("*      *         *      *\n");
		gotoxy(x,y++); printf("    *   *       *   *    \n");
		gotoxy(x,y++); printf("*  *  *  *  *  *  *  *  *\n");
		gotoxy(x,y++); printf("          *   *          \n");
		gotoxy(x,y++); printf("   *       * *       *   \n");
		gotoxy(x,y++); printf("        *   *   *        ");
		
		FLASH();
		y = 10;
		
		CLEAR_SCREEN();
	}
	
	system("COLOR 0F");	
}

extern void potion_of_power(void){
	int x = 39, y = 8;
	register int i;
	
	for(i = 0; i < 21; i++){
		if((i % 2) == 0)
			system("COLOR 0A");
		else
			system("COLOR 02");
	
	
		gotoxy(x,y++); printf("     *********     \n");
		gotoxy(x,y++); printf("	     *	   *      \n");
		gotoxy(x,y++); printf("	     *	   *      \n");
		gotoxy(x,y++); printf("      *	   *      \n");
		gotoxy(x,y++); printf("	     *	   *  	  \n");
		gotoxy(x,y++); printf("	     *     *      \n");
		gotoxy(x,y++); printf("	     *     *      \n");
		gotoxy(x,y++); printf("	     *     *	  \n");
		gotoxy(x,y++); printf("	    *   *   *     \n");
		gotoxy(x,y++); printf("	   *   ***   *    \n");
		gotoxy(x,y++); printf("   *   * * *   *   \n");
		gotoxy(x,y++); printf("  *   *  *  *   *  \n");
		gotoxy(x,y++); printf(" *       *       * \n");
		gotoxy(x,y++); printf("*        *        *\n");
		gotoxy(x,y++); printf("*******************");
		
		FLASH();
		y = 8;
		
		CLEAR_SCREEN();
	}
}

extern void potion_of_healing(void){
	int x = 40, y = 8;
	register int i;
	
	for(i = 0; i < 21; i++){
		if((i % 2) == 0)
			system("COLOR 0A");
		else
			system("COLOR 02");
	
		gotoxy(x,y++); printf("     **********     \n");
		gotoxy(x,y++); printf("      *      *      \n");
		gotoxy(x,y++); printf("      *      *      \n");
		gotoxy(x,y++); printf("      *      *      \n");
		gotoxy(x,y++); printf("      *      *	   \n");
		gotoxy(x,y++); printf("      *      *      \n");
		gotoxy(x,y++); printf("      *      *      \n");
		gotoxy(x,y++); printf("      *      *	   \n");
		gotoxy(x,y++); printf("     *        *     \n");
		gotoxy(x,y++); printf("    *    **    *    \n");
		gotoxy(x,y++); printf("   *     **     *   \n");
		gotoxy(x,y++); printf("  *  **********  *  \n");
		gotoxy(x,y++); printf(" *       **       * \n");
		gotoxy(x,y++); printf("*        **        *\n");
		gotoxy(x,y++); printf("********************");
		
		FLASH();
		y = 8;
		
		CLEAR_SCREEN();
	}
}

extern void magic_rifle(void){
	int x = 28, y = 12;
	register int i;
	
	for(i = 0; i < 21; i++){
		if((i % 2) == 0)
			system("COLOR 0A");
		else
			system("COLOR 02");
		
		gotoxy(x,y++); printf("		     |*************|                    \n");
		gotoxy(x,y++); printf("********************************************|\n");
		gotoxy(x,y++); printf("*        ***********************************|\n");
		gotoxy(x,y++); printf("*	 ****   * *                             \n");
		gotoxy(x,y++); printf("****       ***                               ");
		
		FLASH();
		y = 12;
		
		CLEAR_SCREEN();
	}
}

extern void rain_of_souls(void){
	int x = 38, y = 9;
	register int i;
	
	for(i = 0; i < 21; i++){
		if((i % 2) == 0)
			system("COLOR 09");
		else
			system("COLOR 01");	
	
		gotoxy(x,y++); printf("     *                  \n");
		gotoxy(x,y++); printf("    * *                 \n");
		gotoxy(x,y++); printf("   *   *                \n");
		gotoxy(x,y++); printf("  *     *         *	   \n");
		gotoxy(x,y++); printf(" *       *       * *    \n");
		gotoxy(x,y++); printf("*         *     *   *   \n");
		gotoxy(x,y++); printf(" *       *     *     *  \n");
		gotoxy(x,y++); printf("   * * *      *       * \n");
		gotoxy(x,y++); printf("             *         *\n");
		gotoxy(x,y++); printf("              *       * \n");
		gotoxy(x,y++); printf("                * * *   ");
		
		FLASH();
		y = 9;
		
		CLEAR_SCREEN();
	}
}

extern void shamanism(void){
	int x = 39, y = 10;
	register int i;
	
	for(i = 0; i < 21; i++){
		if((i % 2) == 0)
			system("COLOR 09");
		else
			system("COLOR 01");	
	
		gotoxy(x,y++); printf("        **   **        \n");
		gotoxy(x,y++); printf("         * * *         \n");
		gotoxy(x,y++); printf("         *   *         \n");
		gotoxy(x,y++); printf("**** * * *   * * * ****\n");
		gotoxy(x,y++); printf("   * * * *   * * * *   \n");
		gotoxy(x,y++); printf("         *   *         \n");
		gotoxy(x,y++); printf("         *   *         \n");
		gotoxy(x,y++); printf("         *   *         \n");
		gotoxy(x,y++); printf("         *   *         \n");
		gotoxy(x,y++); printf("          ***          ");
	
		FLASH();
		y = 10;
		
		CLEAR_SCREEN();
	}
}

extern void war_heart(void){
	int x = 31, y = 9;
	register int i;

	for(i = 0; i < 21; i++){
		if((i % 2) == 0)
			system("COLOR 0D");
		else
			system("COLOR 05");	

		gotoxy(x,y++); printf("	       *    *           *    *        \n");
		gotoxy(x,y++); printf("   *             *   *             *   \n");
		gotoxy(x,y++); printf(" *                 *                 * \n");
		gotoxy(x,y++); printf("*                                     *\n");
		gotoxy(x,y++); printf(" *                 *                 * \n");
		gotoxy(x,y++); printf("  *                *                *  \n");
		gotoxy(x,y++); printf("    *              *              *    \n");
		gotoxy(x,y++); printf("      *            *            *      \n");
		gotoxy(x,y++); printf("        *       *******       *        \n");
		gotoxy(x,y++); printf("           *       *       *           \n");
		gotoxy(x,y++); printf("              *         *              \n");
		gotoxy(x,y++); printf("                *     *                \n");
		gotoxy(x,y++); printf("                   *                   ");
	
		FLASH();
		y = 9;
		
		CLEAR_SCREEN();
	}
}

extern void heart_of_courage(void){
	int x = 31, y = 9;
	register int i;
	
	for(i = 0; i < 21; i++){
		if((i % 2) == 0)
			system("COLOR 0D");
		else
			system("COLOR 05");

		gotoxy(x,y++); printf("        *    *           *    *        \n");
		gotoxy(x,y++); printf("   *             *   *             *   \n");
		gotoxy(x,y++); printf(" *                 *                 * \n");
		gotoxy(x,y++); printf("*                                     *\n");
		gotoxy(x,y++); printf(" *              * * * *              * \n");
		gotoxy(x,y++); printf("  *             *     *             *  \n");
		gotoxy(x,y++); printf("    *           *     *           *    \n");
		gotoxy(x,y++); printf("      *         *     *         *      \n");
		gotoxy(x,y++); printf("        *        *   *        *        \n");
		gotoxy(x,y++); printf("           *      ***      *           \n");
		gotoxy(x,y++); printf("              *         *              \n");
		gotoxy(x,y++); printf("                *     *                \n");
		gotoxy(x,y++); printf("                   *                   ");
		
		FLASH();
		y = 9;
		
		CLEAR_SCREEN();
	}
}

extern void willbreaker(void){
	int x = 44, y = 8;
	register int i;
	
	for(i = 0; i < 21; i++){
		if((i % 2) == 0)
			system("COLOR 0E");
		else
			system("COLOR 06");	

		gotoxy(x,y++); printf("     *     \n");
		gotoxy(x,y++); printf("    * *    \n");
		gotoxy(x,y++); printf("    * *    \n");
		gotoxy(x,y++); printf("    * *    \n");
		gotoxy(x,y++); printf("    * *    \n");
		gotoxy(x,y++); printf("    * *	  \n");
		gotoxy(x,y++); printf("    * *    \n");
		gotoxy(x,y++); printf("    * *	  \n");
		gotoxy(x,y++); printf("    * *    \n");
		gotoxy(x,y++); printf("    * *    \n");
		gotoxy(x,y++); printf("    * *    \n");
		gotoxy(x,y++); printf("***********\n");
		gotoxy(x,y++); printf("***********\n");
		gotoxy(x,y++); printf("    ***    \n");
		gotoxy(x,y++); printf("    ***    ");
		
		FLASH();
		y = 8;
		
		CLEAR_SCREEN();
	}
}

extern void titanskin(void){
	int x = 44, y = 10;
	register int i;
	
	for(i = 0; i < 21; i++){
		if((i % 2) == 0)
			system("COLOR 0E");
		else
			system("COLOR 06");	

		gotoxy(x,y++); printf("* * * * * * *\n");
		gotoxy(x,y++); printf("*           *\n");
		gotoxy(x,y++); printf("*     *     *\n"); 
		gotoxy(x,y++); printf("*   *****   *\n");
		gotoxy(x,y++); printf("*     *     *\n"); 
		gotoxy(x,y++); printf("*  *******  *\n");
		gotoxy(x,y++); printf("*     *     *\n");
		gotoxy(x,y++); printf(" *    *    * \n");
		gotoxy(x,y++); printf("  *       *  \n");
		gotoxy(x,y++); printf("    * * *    ");
		
		FLASH();
		y = 10;
		
		CLEAR_SCREEN();
	}
}

extern void swords(void){
	int x = 39, y = 11;
	
	system("COLOR 0C");	
	
	gotoxy(x,y++); printf("   *         * * * * *\n");
	gotoxy(x,y++); printf("   *         *       *\n");
	gotoxy(x,y++); printf("   *         *       *\n");
	gotoxy(x,y++); printf("   *         *       *\n");
	gotoxy(x,y++); printf("   *         *       *\n");
	gotoxy(x,y++); printf("*******      *       *\n");
	gotoxy(x,y++); printf("   *          *     * \n");
	gotoxy(x,y++); printf("   *             *    ");		
}

extern void exit_message(void){
	int x = 42, y = 12;

	gotoxy(x,y++); printf("   **     **   \n");
	gotoxy(x,y++); printf("   **     **   \n");
	gotoxy(x,y++); printf("               \n");
	gotoxy(x,y++); printf("    *  *  *    \n");
	gotoxy(x,y++); printf(" *           * \n");
	gotoxy(x,y++); printf("*             *");
}