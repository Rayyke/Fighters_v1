/*
cizmar.peter.z3.c

Bojovnici 1.1 BETA
==================

Peter Cizmar 3.4.2016
*/

/* Za zadnych okolnosti nemazte prilozeny priecinok 'saves', inak bude fatal error pri zapise dat!
*/

/* Ak ste stahovali tieto subory z AIS a chcete aj hudbu, treba si ju stiahnut dodatocne na tomto linku, a vlozit do foldru kde su loadovacie subory
     - http://uloz.to/xRQebXQS/warriors-songs-zip
	 - heslo: feinomore
*/

/* Trocha som pozmenil 5. bod zadania: "Vojak, ktory prezil suboj prechadza do utoku. Hrac, ktoremu padol vojak, vysle do boja obrancu."
   Zdalo sa mi neprakticke posielat automaticky vojakov do utoku, preto do utoku neprechadza vojak ktory prezil, ale samotny hrac, t.j.
   ma moznost vyberu vojaka ktoreho posle. Je to aj ovela rozumnejsie vzhladom na to ake bonusy som popridaval.		
*/

#include "ultimate_header.h"

int main(void){	
	adjust_window_size(); //prisposobenie obrazovky (nutnost maximalizovat po spusteni)
	hide_cursor(); //neukazovanie blikajuceho otravneho kurzora
	
	srand((int)time(NULL));
	
	language_selection();
	
	return 0;
}

extern void language_selection(void){
	SETUP_SETTINGS setup;
	char input;
	
	system("COLOR 0F"); // cierne pozadie, biely text
	
	printf("Vyber jazyk / choose language:\n\nSVK [s]\nENG [e]");
	
	input = _getch();
	
	//riesi neplatny vstup
	while(input != 's' && input != 'e'){
		printf("\n\nNeplatny vstup, zadaj znovu!\nWrong input, enter the value again!\a"); // "\a" - zvuk po zadani neplatneho cisla (nic nerobi v Linuxe casto!)
		input = _getch();
	}
	
	if(input == 's'){
		setup.language = 0;
		strcpy(setup.all_stats.language, "SVK");
	}
		
	else if(input == 'e'){
		setup.language = 1;
		strcpy(setup.all_stats.language, "ENG");
	}
			
	CLEAR_SCREEN();
	
	basic_setup(setup);
}

extern void help(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup){
	char input;
	CLEAR_SCREEN();
	
	//printf("-> %s\n", (setup.language == 0) ? "" : "");

	printf("%s\n", (setup.language == 0) ? "Pravidla hry" : "Rules of game");
	printf("===================================================================================================="); //100
	printf("-> %s\n", (setup.language == 0) ? "Hrac na zaciatku hry dostane 5 vojakov": "Players will receive 5 units at the beginning");
	printf("-> %s\n", (setup.language == 0) ? "Raz za hru sa hadze mincou, kto tipne spravne, vyhra 1 dusu" : 
											  "Once per game, a coin is tossed, the right guess wins 1 soul");
	printf("-> %s\n", (setup.language == 0) ? "Hraci vzdy bojuju len s jednym bojovnikom" : "Players always fight only with one unit");
	printf("-> %s\n", (setup.language == 0) ? "Raz za hru, hraci maju moznost vymenit utocnika po vybere obancu" : 
											  "Once per game, players have chance to swap attacking units");
	printf("-> %s\n", (setup.language == 0) ? "Boj prebieha, pokym niekto neumrie" : "Fight lasts, until someone dies");
	printf("-> %s\n", (setup.language == 0) ? "Hrac, ktoremu prezije, vybera dalej utocnika" : "The player, whose unit survived, choose the attacker");
	printf("-> %s\n", (setup.language == 0) ? "Pred kazdym bojom je na vyber z roznych bonusov" : "Before every fight, many bonuses may be chosen");
	printf("-> %s\n", (setup.language == 0) ? "Kto prvy nebuda mat bojovnikov, prehral" : "The player with out units, lost");
	printf("-> %s\n\n\n", (setup.language == 0) ? "Informacie o hre najdete v textovom subore v adresary hry" : 
												  "You can find info about the game in the game s directory");

	printf("%s\n", (setup.language == 0) ? "Pre pokracovanie stlac lubovolnu klavesu..." : "Press any key to continue...");
	input = _getch();

	CLEAR_SCREEN();

	printf("%s\n", (setup.language == 0) ? "Bonus - Duse" : "Bonus - Souls");
	printf("===================================================================================================="); //100
	printf("-> %s\n", (setup.language == 0) ? "Hraci ziskavaju duse pocas hry" : "Players receive souls during the game");
	printf("-> %s\n", (setup.language == 0) ? "Ziskat ich je mozno bud z mince alebo zo svojich padlych bojovnikov" : 
											  "Obtaining the souls is possible via the coin toss or the dead units");
	printf("-> %s\n", (setup.language == 0) ? "Ked mas duse, mozes nakupovat bonusy" : "Once you have some souls, you purchase bonuses");
	printf("-> %s\n", (setup.language == 0) ? "Bonusy sa daju nakupit vzdy pred bojom" : "You can buy bonuses always before the fight");
	printf("-> %s\n", (setup.language == 0) ? "Mozes kupit aj viacej bonusov naraz" : "You can buy more than one bonus at the same time");
	printf("-> %s\n\n", (setup.language == 0) ? "Duse su neprenosne z hry do hry, radim ich pouzit v tejto" : "Souls are not transferable from game to game");

	printf("%s\n", (setup.language == 0) ? "Bonus - Demoni" : "Bonus - Demons");
	printf("===================================================================================================="); //100
	printf("-> %s\n", (setup.language == 0) ? "Raz za hru ma hrac moznost privolat demona" : "Once per game, player has chance to summon demon");
	printf("-> %s\n", (setup.language == 0) ? "Hrac ma tuto moznost si vybrat tam kde je nakup za duse" : 
											  "Player can choose this option when he purchases for souls");
	printf("-> %s\n", (setup.language == 0) ? "Obeta je na ukor jedneho bojovnika! (zomrie)" : "Sacrifice is deadly for one unit.");
	printf("-> %s\n", (setup.language == 0) ? "Po obete, je moznost vybrat si z dvoch demonov" : "After ritual, player can choose from 2 demons");
	printf("-> %s\n", (setup.language == 0) ? "Prvy demon znizuje utok nepriatelov o jeden (ak je vyssi ako 1)" : "First decreases the enemy units attack (if higher than 1)");
	printf("-> %s\n\n\n", (setup.language == 0) ? "Druhy demon znizuje obranu nepriatelov o jeden (ak je vyssia ako 1)" : "Second decreases the enemy units defence (if higher than 1)");
	
	printf("%s\n", (setup.language == 0) ? "Pre pokracovanie stlac lubovolnu klavesu..." : "Press any key to continue...");
	input = _getch();

	CLEAR_SCREEN();

	printf("%s\n", (setup.language == 0) ? "Uspechy" : "Achievments");
	printf("===================================================================================================="); //100
	printf("-> %s\n", (setup.language == 0) ? "Hrac ma moznost pocas hrier ziskavat rozne uspechy" : "Players have chance to win achievments during the games");
	printf("-> %s\n", (setup.language == 0) ? "Ziskane achievmenty sa ulozia na konci hry do suboru" : "Obained achievments are save to text file");
	printf("-> %s\n", (setup.language == 0) ? "Kazdy hrac ma svoj subor, ak hrac hra prvy krat, vytvori sa uplne novy" : 
											  "Every player has his own file, if he plays the first time, new one is created");
	printf("-> %s\n", (setup.language == 0) ? "Ak hra znovu pod tym istym meno, nacitaju sa uspechy zo suboru" : 
											  "If he plays again with his name, achievments are loaded from file");
	printf("-> %s\n", (setup.language == 0) ? "Zobrazia sa uspechy v okne aj z predchadzajucich hier" : "Previous achievments are also showed");
	printf("-> %s\n", (setup.language == 0) ? "Dokopy je 15 uspechov, tak sup sup skusat a zbierat" : "There are 15 achievments all together, have fun");
	printf("-> %s\n", (setup.language == 0) ? "Uspechy su obodovane podla narocnosti ziskania" : "Achievments have their own points measurement");
	printf("-> %s\n\n\n", (setup.language == 0) ? "Sucet tychto bodov sa vypisuje vzdy po hre pri uspechoch" : 
											  "These points are always displayed together with achievments");

	printf("%s\n", (setup.language == 0) ? "Pre pokracovanie do menu stlac lubovolnu klavesu..." : "Press any key to continue to menu...");
	input = _getch();
	
	load(player_1, player_2, setup);
}

extern void load(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup){
	char input;	

	if (setup.music_allowed == 1) {
		mciSendString("open intro_music.mp3 alias intro_music", NULL, 0, 0);
		mciSendString("play intro_music notify repeat", NULL, 0, 0);
	}
	
	CLEAR_SCREEN();	
	
	printf("%s\t[s]\n", (setup.language == 0) ? "Spusti hru" : "Start game");
	printf("%s\t[%s]\n", (setup.language == 0) ? "Ukonceni hru" : "End game", (setup.language == 0) ? "u" : "e");
	printf("%s\t\t[%s]", (setup.language == 0) ? "Pomoc" : "Help", (setup.language == 0) ? "p" : "h");
	
	input = _getch();
	
	while(input != 's' && input != ((setup.language == 0) ? 'u' : 'e') && input != ((setup.language == 0) ? 'p' : 'h')){
		ERR_MSG;
		input = _getch();
	}
	
	if(input == 'n'){
		setup.timer_game[0] = clock();
	}
	else if(input == ((setup.language == 0) ? 'u' : 'e')){
		mciSendString("stop intro_music", NULL, 0, 0);
		mciSendString("close intro_music", NULL, 0, 0);

		EXIT;
	}	
	else if(input == ((setup.language == 0) ? 'p' : 'h')){
		help(player_1, player_2, setup);
	}
	
	mciSendString("stop intro_music", NULL, 0, 0);
	mciSendString("close intro_music", NULL, 0, 0);

	mciSendString("open main_music.mp3 alias main_music", NULL, 0, 0);
	mciSendString("play main_music notify repeat", NULL, 0, 0);
		
	CLEAR_SCREEN();
	intro_text(setup);
	LONGER_PAUSE();
	CLEAR_SCREEN();
	SHORT_PAUSE();
	CLEAR_SCREEN();	
	
	if(setup.presentation_mode == 1){
		setup.abilities = 1;
		strcpy(setup.all_stats.allowed_bonuses, (setup.language == 0) ? "ANO" : "YES");
	}
	else if(setup.presentation_mode == 0){
		printf("%s\n\n%s\t[%s]\n%s\t[%s]", (setup.language == 0) ? "Povolit bonusy?" : "Allow bonuses?", 
									   	   (setup.language == 0) ? "Ano" : "Yes", (setup.language == 0) ? "a" : "y",
									   	   (setup.language == 0) ? "Nie" : "No", "n");
	
		input = _getch();
		
		//riesi neplatny vstup
		while(input != ((setup.language == 0) ? 'a' : 'y') && input != 'n'){
			printf("\n\nNeplatny vstup, zadaj znovu!\nWrong input, enter the value again!\a");
			input = _getch();
		}
		
		if(input == ((setup.language == 0) ? 'a' : 'y')){
			setup.abilities = 1;
			strcpy(setup.all_stats.allowed_bonuses, (setup.language == 0) ? "ANO" : "YES");
		}
			
		else if(input == 'n'){
			setup.abilities = 0;
			strcpy(setup.all_stats.allowed_bonuses, (setup.language == 0) ? "NIE" : "NO");
		}
		
		CLEAR_SCREEN();
	}
	
	printf("%s", (setup.language == 0) ? "Zadaj meno hraca 1: " : "Type a name of the 1st player: ");
	fgets(player_1.name, MAX_NAME_LENGTH, stdin); 
	
	//odstranenie automatickeho dalsieho riadku za fgets()
	if (player_1.name[strlen(player_1.name) - 1] == '\n')
    	player_1.name[strlen(player_1.name) - 1] = '\0';
	
	if(setup.abilities == 1){
		if(strcmp(player_1.name, "Souleater") == 0){ // porovnanie, 0 zhoda, 1 nezhoda
			player_1.souls = 3;
			player_1.achievments.easter_egg++;
		}
	}
	
	CLEAR_SCREEN();
	printf("%s", (setup.language == 0) ? "Zadaj meno hraca 2: " : "Type a name of the 2nd player: ");
	fgets(player_2.name, MAX_NAME_LENGTH, stdin);
	
	if (player_2.name[strlen(player_2.name) - 1] == '\n')
    	player_2.name[strlen(player_2.name) - 1] = '\0';

	// kontrola mena na zhodu s prvym
	while(strcmp(player_2.name, player_1.name) == 0){
		printf("\n%s", (setup.language == 0) ? "To iste meno ako hrac 1! Zadaj ine meno hraca 2: " : 
											   "The same name as player 1! Type another name of player 2: ");
		fgets(player_2.name, MAX_NAME_LENGTH, stdin);

		if (player_2.name[strlen(player_2.name) - 1] == '\n')
			player_2.name[strlen(player_2.name) - 1] = '\0';
	}
	
	if(setup.abilities == 1){
		if(strcmp(player_2.name, "Souleater") == 0){
			player_2.souls = 3;
			player_2.achievments.easter_egg++;
		}		
	}
	
	CLEAR_SCREEN();
	
	units_generator(player_1, player_2, setup);
}

extern void units_generator(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup){
	register int i; // register - casto sa meniaca premenna, zapis len do registra..setri ukonovy cas
	char *names[MAX_UNITS_TOTAL] = {"Darkmace","Ethernal","Archfire","Phantom","Chrono","Yolt","Grounder","Faceless","Mer","Shadow"};
	UNIT_STATS units_all[MAX_UNITS_TOTAL]; // k jednotkam pristupujem cez pole, zatial nepatria ziadnemu hracovi
	
	for(i = 0; i < MAX_UNITS_TOTAL; i++){
		strcpy(units_all[i].name, names[i]); // priradim mena z "names"	
			
		// priradim max hp, attack a defence	
		units_all[i].health_pool = 5;
		units_all[i].attack	= rand() % 4 + 1; // utok min 1 a max 4 (5 nemoze byt ani utok ani obrana), sucet utok a obrany je vzdy 5
		units_all[i].defence = 5 - units_all[i].attack;		
	}
	
	if(setup.presentation_mode == 1){
		for(i = 0; i < 6; i++)
			units_all[i].health_pool = 2;
			
		for(i = 6; i < 10; i++)	
			units_all[i].health_pool = 0;
			
		player_1.souls += 1;
		player_2.souls += 1;
	}
			
	units_sorting(units_all, player_1, player_2, setup);
}

extern void units_sorting(UNIT_STATS units_all[], PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup){
	char input;
	register int i, j;
	int player_1_chosen_units[MAX_UNITS_TOTAL/2]; // units hraca 1, iba oznacenie
	int player_2_chosen_units[MAX_UNITS_TOTAL/2]; // units hraca 2, iba oznacenie
	int chosen_units[MAX_UNITS_TOTAL] = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1}; //cislo musi byt ine ako 0-9, pole oznacuje uz vybranne jednotky
	int counter_player_1 = 1, counter_player_2 = 1;
	
	// hraci si vyberaju units
	for(i = 0; i < MAX_UNITS_TOTAL; i++){
		if(i == 0 || i == 3 || i == 4 || i == 7 || i == 8) // ferove rozdelovanie (A: 1, B: 2, A: 2, B: 2, A: 2, B: 1) ... nezvyhodnuje hraca c.1
			printf("%s%s%i%s\n\n\n", player_1.name , (setup.language == 0) ? ", vyber si " : ", choose the ", counter_player_1, 
													 (setup.language == 0) ? ". jednotku!" : ". unit!");
		else
			printf("%s%s%i%s\n\n\n", player_2.name , (setup.language == 0) ? ", vyber si " : ", choose the ", counter_player_2, 
													 (setup.language == 0) ? ". jednotku!" : ". unit!");
	
		// vypis zostavajucich units
		printf("%s", (setup.language == 0) ? "     Meno\t\tZivoty\tUtok\tObrana\n" : "     Name\t\tHP\tAttack\tDefence\n");
		printf("%s", (setup.language == 0) ? "==============================================\n" : 
											 "===============================================\n"); //o jedno viacej "="
		
		for(j = 0; j < MAX_UNITS_TOTAL; j++){
			if(j != chosen_units[0] && j != chosen_units[1] && j != chosen_units[2] && 
			   j != chosen_units[3] && j != chosen_units[4] && j != chosen_units[5] && 
			   j != chosen_units[6] && j != chosen_units[7] && j != chosen_units[8] && 
			   j != chosen_units[9])
			   
				printf("[%i]  %s\t\t%i\t%i\t%i\n", j, units_all[j].name, units_all[j].health_pool, units_all[j].attack, units_all[j].defence);
		} 
				
		input = _getch();
		
		// riesi neplatny vstup, ak je iny ako 0-9 a ak uz bola unit vybrana
		while(input - ASCII_NUM_VAL < 0 || input - ASCII_NUM_VAL > 9 ||
		      input - ASCII_NUM_VAL == chosen_units[0] || input - ASCII_NUM_VAL == chosen_units[1] ||
		      input - ASCII_NUM_VAL == chosen_units[2] || input - ASCII_NUM_VAL == chosen_units[3] ||
		      input - ASCII_NUM_VAL == chosen_units[4] || input - ASCII_NUM_VAL == chosen_units[5] ||
		      input - ASCII_NUM_VAL == chosen_units[6] || input - ASCII_NUM_VAL == chosen_units[7] ||
		      input - ASCII_NUM_VAL == chosen_units[8] || input - ASCII_NUM_VAL == chosen_units[9]){
		      	
			ERR_MSG;
			input = _getch(); 
		}
		
		if(i == 0 || i == 3 || i == 4 || i == 7 || i == 8)
			player_1_chosen_units[counter_player_1++ - 1] = input - ASCII_NUM_VAL;
		else
			player_2_chosen_units[counter_player_2++ - 1] = input - ASCII_NUM_VAL;
		
		chosen_units[i] = input - ASCII_NUM_VAL;
		
		CLEAR_SCREEN();
	}
	
	// prerozdelenie kazdy hrac vlastni 5 bojovnikov v poli, kde kazda bunka ma vlastnost struktury(t.j. name, health_pool, attack, defence)
	for(i = 0; i < MAX_UNITS_TOTAL/2; i++){
		player_1.army[i] = units_all[player_1_chosen_units[i]];
		player_2.army[i] = units_all[player_2_chosen_units[i]];	
	}
	
	show_player_1_units_status(player_1, setup);
	printf("\n\n");
	show_player_1_units_status(player_2, setup);
	
	printf("\n\n%s", (setup.language == 0) ? "Pre zahajenie boja stlac lubovolnu klavesu!" : "Press any key to fight!");
	
	input = _getch(); // nacitanie za ucelom zdrzania programu...elegantnejsia a menej nasilna forma system("pause")
	
	//vyhodnotenie priemernej attack_ a defence_power hraca 1
	for(i = 0; i < 5; i++)
		setup.all_stats.average_attack_power_p1_generated += player_1.army[i].attack;
	setup.all_stats.average_attack_power_p1_generated /= 5.0;
	
	for(i = 0; i < 5; i++)
		setup.all_stats.average_defence_power_p1_generated += player_1.army[i].defence;
	setup.all_stats.average_defence_power_p1_generated /= 5.0;
	
	//vyhodnotenie priemernej attack_ a defence_power hraca 2
	for(i = 0; i < 5; i++)
		setup.all_stats.average_attack_power_p2_generated += player_2.army[i].attack;
	setup.all_stats.average_attack_power_p2_generated /= 5.0;
	
	for(i = 0; i < 5; i++)
		setup.all_stats.average_defence_power_p2_generated += player_2.army[i].defence;
	setup.all_stats.average_defence_power_p2_generated /= 5.0;
	
	
	play(player_1, player_2, setup);		
}

extern void play(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup){
	char input;
	
	check_winner(player_1, player_2, setup); 
	
	if(setup.abilities == 1){ //specialne bonusy musia byt povolene  
		if(setup.first_round == 0){
			setup.first_round = 1;
			coin_of_dark_pact(player_1, player_2, setup); //hod mincou na zaciatku hry...hrac 1 vyhral: 1, hrac 2 vyhral: 2	
		}
	
		// hrac 1 vybera nejaku specialnu schopnost/-i este pred bojom dvoch jednotiek
		if(setup.abilities_allow[0]++ == 0) // vykonaj raz pred bojom, je tam jedna rekurzia ktory by to bez podmienky zacyklila, znova povolim specialne schopnosti pre hraca 1 ked skonci boj vo fight()
			player_1_special_abilities(player_1, player_2, setup);
			
		// hrac 2 vybera nejaku specialnu schopnost/-i este pred bojom dvoch jednotiek
		if(setup.abilities_allow[1]++ == 0) // vykonaj raz pred bojom, je tam jedna rekurzia ktory by to bez podmienky zacyklila, znova povolim specialne schopnosti pre hraca 2 ked skonci boj vo fight()
			player_2_special_abilities(player_1, player_2, setup);	
	}
		
	// vybera sa utocny bojovnik
	CLEAR_SCREEN();
	
	if(setup.who_attacks == 1){
		printf("%s%s\n\n",player_1.name, (setup.language == 0) ? ", vyber si bojovnika do utoku!" : ", choose the unit for attacking!");	
		
		show_player_1_units_selection(player_1, setup);	
		
		input = _getch();
	
		while(input - ASCII_NUM_VAL < 0 || input - ASCII_NUM_VAL > 4 || player_1.army[input - ASCII_NUM_VAL].health_pool == 0){
			ERR_MSG_OR_DEAD;
			input = _getch();
		}			
	 			
		setup.active_unit[0] = input - ASCII_NUM_VAL;
	} 	
	else if(setup.who_attacks == 2){
		printf("%s%s\n\n",player_2.name, (setup.language == 0) ? ", vyber si bojovnika do utoku!" : ", choose the unit for attacking!");	
		
		show_player_2_units_selection(player_2, setup);	
		
		input = _getch();
	
		while(input - ASCII_NUM_VAL < 0 || input - ASCII_NUM_VAL > 4 || player_2.army[input - ASCII_NUM_VAL].health_pool == 0){
			ERR_MSG_OR_DEAD;
			input = _getch();
		}			
	 			
		setup.active_unit[1] = input - ASCII_NUM_VAL;
	}
			
	// vybera sa obranujuci bojovnik
	CLEAR_SCREEN();
		
	if(setup.who_attacks == 1){
		printf("%s%s\n\n",player_2.name, (setup.language == 0) ? ", vyber si bojovnika do obrany!" : ", choose the unit for defending!");	
		
		show_player_2_units_selection(player_2, setup);	
	
		input = _getch();
		
		while(input - ASCII_NUM_VAL < 0 || input - ASCII_NUM_VAL > 4 || player_2.army[input - ASCII_NUM_VAL].health_pool == 0){
			ERR_MSG_OR_DEAD;
			input = _getch();
		}
		
		setup.active_unit[1] = input - ASCII_NUM_VAL;
	}
	else if(setup.who_attacks == 2){
		printf("%s%s\n\n",player_1.name, (setup.language == 0) ? ", vyber si bojovnika do obrany!" : ", choose the unit for attacking!");	
		
		show_player_1_units_selection(player_1, setup);	
	
		input = _getch();
		
		while(input - ASCII_NUM_VAL < 0 || input - ASCII_NUM_VAL > 4 || player_1.army[input - ASCII_NUM_VAL].health_pool == 0){
			ERR_MSG_OR_DEAD;
			input = _getch();
		}
		
		setup.active_unit[0] = input - ASCII_NUM_VAL;
	}
	
	// moznosti swapov bojovnikov raz za hru, zvlast pre kazdeho hraca ako vsetko
	if(setup.abilities == 1 && setup.who_attacks == 1 && player_1.swap_available == 1 && player_1.alive_units > 1){
		CLEAR_SCREEN();
		
		printf("%s%s\n\n",player_1.name, (setup.language == 0) ? ", mas moznost zmenit utocnika raz za hru!\nChces tu moznost teraz vyuzit?" : 
																 ", you have a chance to swap attacking unit once per game!\nDo you want to use this feature now?");
																	 
		printf("%s\t[%s]\n%s\t[%s]", (setup.language == 0) ? "Ano" : "Yes", (setup.language == 0) ? "a" : "y",
									 (setup.language == 0) ? "Nie" : "No", "n");
	
		input = _getch();
			
		while(input != ((setup.language == 0) ? 'a' : 'y') && input != 'n'){
			printf("\n\nNeplatny vstup, zadaj znovu!\nWrong input, enter the value again!\a");
			input = _getch();
		}
		
		CLEAR_SCREEN();	
			
		if(input == ((setup.language == 0) ? 'a' : 'y')){
			player_1.achievments.no_bonus_baby = 0;
			setup.all_stats.swap_used++;
			
			printf("%s%s\n\n",player_1.name, (setup.language == 0) ? ", vyber si nahradu do utoku!" : ", choose the replacement for the attack!");
				
			show_player_1_units_selection(player_1, setup);	
		
			input = _getch();
		
			while(input - ASCII_NUM_VAL < 0 || input - ASCII_NUM_VAL > 4 || 
				  player_1.army[input - ASCII_NUM_VAL].health_pool == 0 || input - ASCII_NUM_VAL == setup.active_unit[0]){
				printf("\n%s\n\a", (setup.language == 0) ? "Neplatny vstup, alebo bojovnik je uz mrtvy, alebo si vybral toho isteho!" : 
														   "Wrong input, or the unit is already dead, or you chose the same unit!");
				input = _getch();
			}			
			
			CLEAR_SCREEN();
			
			gotoxy(31, 14);
			printf("%s%s(%s)", player_1.army[input - ASCII_NUM_VAL].name, (setup.language == 0) ? " nahradil povodneho utocnika! " : 
																								  " replaced previous attacker! ",
							   player_1.army[setup.active_unit[0]].name);
							   
			LONG_PAUSE(); 
			CLEAR_SCREEN();
			SHORT_PAUSE();				   		 			
		 			
			setup.active_unit[0] = input - ASCII_NUM_VAL;
			player_1.swap_available = 0;
			player_1.achievments.brothers_in_war++;
			strcpy(setup.all_stats.swap_used_p1, (setup.language == 0) ? "ANO" : "YES");
		}	
		else
		;												 		
	}
	
	if(setup.abilities == 1 && setup.who_attacks == 2 && player_2.swap_available == 1 && player_2.alive_units > 1){
		CLEAR_SCREEN();
		
		printf("%s%s\n\n",player_2.name, (setup.language == 0) ? ", mas moznost zmenit utocnika raz za hru!\nChces tu moznost teraz vyuzit?" : 
																 ", you have a chance to swap attacking unit once per game!\nDo you want to use this feature now?");
																	 
		printf("%s\t[%s]\n%s\t[%s]", (setup.language == 0) ? "Ano" : "Yes", (setup.language == 0) ? "a" : "y",
									 (setup.language == 0) ? "Nie" : "No", "n");
	
		input = _getch();
			
		while(input != ((setup.language == 0) ? 'a' : 'y') && input != 'n'){
			printf("\n\nNeplatny vstup, zadaj znovu!\nWrong input, enter the value again!\a");
			input = _getch();
		}	
		
		CLEAR_SCREEN();
			
		if(input == ((setup.language == 0) ? 'a' : 'y')){
			player_2.achievments.no_bonus_baby = 0;
			setup.all_stats.swap_used++;
			
			printf("%s%s\n\n",player_2.name, (setup.language == 0) ? ", vyber si nahradu do utoku!" : ", choose the replacement for the attack!");
				
			show_player_2_units_selection(player_2, setup);	
		
			input = _getch();
		
			while(input - ASCII_NUM_VAL < 0 || input - ASCII_NUM_VAL > 4 || 
				  player_2.army[input - ASCII_NUM_VAL].health_pool == 0 || input - ASCII_NUM_VAL == setup.active_unit[1]){
				printf("\n%s\n\a", (setup.language == 0) ? "Neplatny vstup, alebo bojovnik je uz mrtvy, alebo si vybral toho isteho!" : 
														   "Wrong input, or the unit is already dead, or you chose the same unit!");
				input = _getch();
			}			
		 			
			CLEAR_SCREEN();
			
			gotoxy(31, 14);
			printf("%s%s(%s)", player_2.army[input - ASCII_NUM_VAL].name, (setup.language == 0) ? " nahradil povodneho utocnika! " : 
																								  " replaced previous attacker! ",
							   player_2.army[setup.active_unit[1]].name);
							   
			LONG_PAUSE(); 
			CLEAR_SCREEN();
			SHORT_PAUSE();				   		 			
		 			
			setup.active_unit[1] = input - ASCII_NUM_VAL;	
			player_2.swap_available = 0;
			player_2.achievments.brothers_in_war++;
			strcpy(setup.all_stats.swap_used_p1, (setup.language == 0) ? "ANO" : "YES");
		}	
		else
		;												 		
	}
		
	fight(player_1, player_2, setup); //zahajenie boja s vybranymi jednotkami
}	

extern void fight(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup){
	int random_pool, random_number;
	char input;
	
	setup.timer_fight[0] = clock();
	
	while(player_1.army[setup.active_unit[0]].health_pool != 0 && player_2.army[setup.active_unit[1]].health_pool != 0){
		CLEAR_SCREEN(); 
		
		gotoxy(42, 14);
		if(setup.who_attacks == 1)
			printf("%s%s", player_1.army[setup.active_unit[0]].name , (setup.language == 0) ? " utoci!" : " is attacking!");
		else if(setup.who_attacks == 2)
			printf("%s%s", player_2.army[setup.active_unit[1]].name , (setup.language == 0) ? " utoci!" : " is attacking!");
			 
		PAUSE_DOTS();
		CLEAR_SCREEN();
		SHORT_PAUSE();
		
		gotoxy(41, 14);	
		if(setup.who_attacks == 1)
			printf("%s%s", player_2.army[setup.active_unit[1]].name , (setup.language == 0) ? " sa brani!" : " is defending!");
		else if(setup.who_attacks == 2)
			printf("%s%s", player_1.army[setup.active_unit[0]].name , (setup.language == 0) ? " sa brani!" : " is defending!");	
		
		PAUSE_DOTS();
		CLEAR_SCREEN(); 
		SHORT_PAUSE();
		swords();
		PAUSE_DOTS();
		CLEAR_SCREEN();
		SHORT_PAUSE();
		
		system("COLOR 0F");	
		
		if(setup.who_attacks == 1)
			random_pool = player_1.army[setup.active_unit[0]].attack + player_2.army[setup.active_unit[1]].defence;
		else if(setup.who_attacks == 2)
			random_pool = player_2.army[setup.active_unit[1]].attack + player_1.army[setup.active_unit[0]].defence;		
	
		random_number = rand() % random_pool + 1;	
		
		if(setup.who_attacks == 1){
			if(random_number > player_2.army[setup.active_unit[1]].defence){
				printf("%s%s%s\n\n", player_1.army[setup.active_unit[0]].name, (setup.language == 0) ? " zasiahol bojovnika " : " hit the ", 
									 player_2.army[setup.active_unit[1]].name);
				player_2.army[setup.active_unit[1]].health_pool--;
				setup.all_stats.hits_p1++;
				
				player_1.achievments.lucky_strike[1]++;
				if(player_1.achievments.lucky_strike[1] == 5)
					player_1.achievments.lucky_strike[0] = 1;
					
				player_1.achievments.bad_luck_brian[1] = 0;
			}
			else{
				printf("%s%s%s\n\n", player_1.army[setup.active_unit[0]].name, (setup.language == 0) ? " nezasiahol bojovnika " : " did not hit the ", 
									 player_2.army[setup.active_unit[1]].name);	
				setup.all_stats.non_hits_p1++;					 
				
				player_1.achievments.bad_luck_brian[1]++;
				if(player_1.achievments.bad_luck_brian[1] == 5)
					player_1.achievments.bad_luck_brian[0] = 1;
					
				player_1.achievments.lucky_strike[1] = 0;	
			}
		}
		else if(setup.who_attacks == 2){
			if(random_number > player_1.army[setup.active_unit[0]].defence){
				printf("%s%s%s\n\n", player_2.army[setup.active_unit[1]].name, (setup.language == 0) ? " zasiahol bojovnika " : " hit the ", 
									 player_1.army[setup.active_unit[0]].name);
				player_1.army[setup.active_unit[0]].health_pool--;
				setup.all_stats.hits_p2++;
				
				player_2.achievments.lucky_strike[1]++;
				if(player_2.achievments.lucky_strike[1] == 5)
					player_2.achievments.lucky_strike[0] = 1;
					
				player_2.achievments.bad_luck_brian[1] = 0;
			}
			else{
				printf("%s%s%s\n\n", player_2.army[setup.active_unit[1]].name, (setup.language == 0) ? " nezasiahol bojovnika " : " did not hit the ", 
									 player_1.army[setup.active_unit[0]].name);	
				setup.all_stats.non_hits_p2++;					 
									 
				player_2.achievments.bad_luck_brian[1]++;
				if(player_2.achievments.bad_luck_brian[1] == 5)
					player_2.achievments.bad_luck_brian[0] = 1;					 
				
				player_2.achievments.lucky_strike[1] = 0;
			}				
		}
		
		if(player_1.army[setup.active_unit[0]].health_pool != 0 && player_2.army[setup.active_unit[1]].health_pool != 0){
			show_units_fight_status(player_1, player_2, setup);
			
			if(setup.who_attacks == 1)
				printf("\n\n%s%s", player_2.name, (setup.language == 0) ? ", stlac lubovolnu klavesu na protiutok!" :
																		  ", press any key to perform counter attack!");
			else if(setup.who_attacks == 2)
				printf("\n\n%s%s", player_1.name, (setup.language == 0) ? ", stlac lubovolnu klavesu na protiutok!" :
																		  ", press any key to perform counter attack!");
			
			input = _getch();
					
			setup.who_attacks = (setup.who_attacks == 1) ? 2 : 1;	// zabezpeci striedave utocenie	
		}
		else{
			CLEAR_SCREEN(); 
			
			if(player_1.army[setup.active_unit[0]].health_pool == 0){
				gotoxy(33, 14);
				printf("%s%s", player_1.army[setup.active_unit[0]].name, (setup.language == 0) ? " padol v boji! (+1 dusa)" : " died in fight! (+1 soul)");
				setup.who_attacks = 2;
				player_1.alive_units--;

				if(setup.cd_rom_eject_allowed == 1){
					mciSendString("open CDAudio", NULL, 0, NULL);            // otvor pristup k zariadeniu
					mciSendString("set CDAudio door open", NULL, 0, NULL);   // otvor dvierka
					mciSendString("close CDAudio", NULL, 0, NULL);
				}
				
				
				if(setup.abilities == 1)
					player_1.souls++;
					
				if(++player_2.achievments.killing_spree[1] > player_2.achievments.killing_spree[0])
					player_2.achievments.killing_spree[0] = player_2.achievments.killing_spree[1];
					
				player_1.achievments.killing_spree[1] = 0;	
			}	
			else if(player_2.army[setup.active_unit[1]].health_pool == 0){
				gotoxy(33, 14);
				printf("%s%s", player_2.army[setup.active_unit[1]].name, (setup.language == 0) ? " padol v boji! (+1 dusa)" : " died in fight! (+1 soul)");
				setup.who_attacks = 1;
				player_2.alive_units--;

				if(setup.cd_rom_eject_allowed == 1) {
					mciSendString("open CDAudio", NULL, 0, NULL);            // otvor pristup k zariadeniu
					mciSendString("set CDAudio door open", NULL, 0, NULL);   // otvor dvierka
					mciSendString("close CDAudio", NULL, 0, NULL);
				}
				
				if(setup.abilities == 1)
					player_2.souls++;
					
				if(++player_1.achievments.killing_spree[1] > player_1.achievments.killing_spree[0])
					player_1.achievments.killing_spree[0] = player_1.achievments.killing_spree[1];
					
				player_2.achievments.killing_spree[1] = 0;		
			}
			
			setup.abilities_allow[0] = 0;
			setup.abilities_allow[1] = 0;
			
			LONG_PAUSE();
			CLEAR_SCREEN(); 
			SHORT_PAUSE();
			grave();
			LONG_PAUSE(); 
			CLEAR_SCREEN(); 
			system("COLOR 0F");
			SHORT_PAUSE();
			
			setup.all_stats.number_of_fights++;
			setup.timer_fight[1] = clock();
			setup.all_stats.fight_durations_total += (setup.timer_fight[1] - setup.timer_fight[0]) / (double)CLOCKS_PER_SEC;
			
			break;
		}	
	}
	
	play(player_1, player_2, setup);			
}

extern void coin_of_dark_pact(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup){
	int coin_toss, winner_of_coin_toss; 
	int players_coin_value[2]; 
	register int i;
	char input;
	
	CLEAR_SCREEN();
	
	printf("%s\n\n", (setup.language == 0) ? "Pred zacatim bojov sa hodi mincou demonickeho paktu.\nSpravny tip vyhrava bonus!" : 
									   		 "Before the starting a fight, coin of the dark pact will be tossed.\nThe right guess wins the bonus!");
	printf("%s%s\n\n%s\t[%s]\n%s\t[%s]", player_1.name, (setup.language == 0) ? ", vyber si svoju stranu mince!" : ", choose your side of the coin!", 
														(setup.language == 0) ? "Panna" : "Front", (setup.language == 0) ? "p" : "f", 
														(setup.language == 0) ? "Orol" : "Back",  (setup.language == 0) ? "o" : "b");
	
	input = _getch();
	
	while(input != ((setup.language == 0) ? 'p' : 'f') && input != ((setup.language == 0) ? 'o' : 'b')){
		ERR_MSG;
		input = _getch();
	}
	
	if(input == ((setup.language == 0) ? 'p' : 'f')){
		players_coin_value[0] = 1;
		players_coin_value[1] = 2;	
	}
	else{
		players_coin_value[0] = 2;
		players_coin_value[1] = 1;	
	}
	
	coin_toss = rand() % 2 + 1;
	
	CLEAR_SCREEN();
	gotoxy(36,14);
	printf("%s", (setup.language == 0) ? "Hadzem mincou " : "Tossing the coin "); //todo postupne bodky
	PAUSE_DOTS();
	
	for(i = 0;i < 3;i++){
		printf(". ");
		PAUSE_DOTS();
	}
	
	if(coin_toss == 1){
		printf("%s", (setup.language == 0) ? "PANNA" : "FRONT");
		strcpy(setup.all_stats.side_of_coin, (setup.language == 0) ? "PANNA" : "FRONT");
	}
		
	else{
		printf("%s", (setup.language == 0) ? "OROL" : "BACK");	
		strcpy(setup.all_stats.side_of_coin, (setup.language == 0) ? "OROL" : "BACK");
	}
		
		
	winner_of_coin_toss = (players_coin_value[0] == coin_toss) ? 1 : 2;
		
	PAUSE();
	SHORT_PAUSE();
	CLEAR_SCREEN();
	SHORT_PAUSE();
	CLEAR_SCREEN();
			
	gotoxy(33,14);
	if(winner_of_coin_toss == 1){
		printf("%s%s", player_1.name, (setup.language == 0) ? ", ziskal si jednu dusu!" : ", you obtained the soul!");
		player_1.souls++;
		player_1.achievments.win_a_coin = 1;
		strcpy(setup.all_stats.winner_of_coin, player_1.name);	
	}
	else{
		printf("%s%s", player_2.name, (setup.language == 0) ? ", ziskal si jednu dusu!" : ", you obtained the soul!");
		player_2.souls++;
		player_2.achievments.win_a_coin = 1;
		strcpy(setup.all_stats.winner_of_coin, player_2.name);	
	}
			
	LONGER_PAUSE();
	CLEAR_SCREEN();
	SHORT_PAUSE();
	CLEAR_SCREEN();
	
	play(player_1, player_2, setup);
}

extern void save_data(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup){
	FILE *fw_stats;
	
	setup.all_stats.units_left_p1 = player_1.alive_units;
	setup.all_stats.units_left_p2 = player_2.alive_units;
	
	//setup.all_stats.total_achievment_points_p1 = player_1.total_achievment_points;
	//setup.all_stats.total_achievment_points_p2 = player_2.total_achievment_points;	
	
	setup.all_stats.hit_rate_p1 = ((double)setup.all_stats.hits_p1 / (double)(setup.all_stats.hits_p1 + setup.all_stats.non_hits_p1)) * 100; // v %
	setup.all_stats.hit_rate_p2 = ((double)setup.all_stats.hits_p2 / (double)(setup.all_stats.hits_p2 + setup.all_stats.non_hits_p2)) * 100; // v %
	
	setup.all_stats.average_attack_defence_rate_p1_generated = setup.all_stats.average_attack_power_p1_generated / setup.all_stats.average_defence_power_p1_generated;
	setup.all_stats.average_attack_defence_rate_p2_generated = setup.all_stats.average_attack_power_p2_generated / setup.all_stats.average_defence_power_p2_generated;
	
	setup.all_stats.souls_spent 							= setup.all_stats.souls_spent_p1 + setup.all_stats.souls_spent_p2;
	setup.all_stats.items_purchased 						= setup.all_stats.items_purchased_p1 + setup.all_stats.items_purchased_p2;
	setup.all_stats.average_attack_power_generated 			= (setup.all_stats.average_attack_power_p1_generated + setup.all_stats.average_attack_power_p2_generated) / 2;
	setup.all_stats.average_defence_power_generated 		= (setup.all_stats.average_defence_power_p1_generated + setup.all_stats.average_defence_power_p2_generated) / 2;
	setup.all_stats.average_attack_defence_rate_generated 	= (setup.all_stats.average_attack_defence_rate_p1_generated + setup.all_stats.average_attack_defence_rate_p2_generated) / 2;
	setup.all_stats.average_fight_duration 					= setup.all_stats.fight_durations_total / (double)setup.all_stats.number_of_fights;
	
	setup.timer_game[1] = clock();
	setup.all_stats.game_duration = (setup.timer_game[1] - setup.timer_game[0]) / (double)CLOCKS_PER_SEC;
	
	
	if((fw_stats = fopen("game_summary.txt", "w")) == NULL){
		gotoxy(22, 14);
		printf("%s", (setup.language == 0) ? "Chyba pri otvarani suboru, preskakujem zapis!" : "Error while opening file, preskakujem zapis!");
		LONG_PAUSE();
		CLEAR_SCREEN();
		SHORT_PAUSE();
		
		basic_setup(setup);
	}

	//samotny zapis do suboru
	fprintf(fw_stats, "%s\n", (setup.language == 0) ? "Celkove statistiky" : "Overall stats");
	fprintf(fw_stats, "====================================================================================================================\n");
	fprintf(fw_stats, "%s\t\t\t\t= %.0fs\n", (setup.language == 0) ? "Hra trvala" : "Game lasted", setup.all_stats.game_duration);
	fprintf(fw_stats, "%s\t\t\t\t= %.0fs\n", (setup.language == 0) ? "Boje trvali" : "Fights lasted", setup.all_stats.fight_durations_total);
	fprintf(fw_stats, "%s\t\t\t= %i\n", (setup.language == 0) ? "Pocet bojov       " : "Number of fights", setup.all_stats.number_of_fights);
	fprintf(fw_stats, "%s\t\t= %.0fs\n", (setup.language == 0) ? "Jeden boj trval v priemere" : "One fight lasted in average", setup.all_stats.average_fight_duration);
	fprintf(fw_stats, "%s\t\t\t\t= %s\n", (setup.language == 0) ? "Jazyk hry" : "Language", setup.all_stats.language);
	fprintf(fw_stats, "%s\t\t\t\t= %s\n", (setup.language == 0) ? "Povolene bonusy" : "Allowed bonuses", setup.all_stats.allowed_bonuses);
	fprintf(fw_stats, "%s\t\t\t\t= %s\n", (setup.language == 0) ? "Vyherca hry" : "Winner of game", setup.all_stats.winner);
	fprintf(fw_stats, "%s\t\t\t= %s\n", (setup.language == 0) ? "Vyherca hadzania mince" : "Winner of coin tossing", setup.all_stats.winner_of_coin);
	fprintf(fw_stats, "%s\t\t\t\t= %s\n", (setup.language == 0) ? "Strana mince" : "Side of coin", setup.all_stats.side_of_coin);
	fprintf(fw_stats, "%s\t\t\t\t= %i\n", (setup.language == 0) ? "Utratene duse" : "Souls spent", setup.all_stats.souls_spent);
	fprintf(fw_stats, "%s\t\t\t= %i\n", (setup.language == 0) ? "Privolany demoni" : "Summoned demons  ", setup.all_stats.summoned_demons);
	fprintf(fw_stats, "%s\t\t\t= %i\n", (setup.language == 0) ? "Vymeneny bojovnici" : "Swapped units    ", setup.all_stats.swap_used);
	fprintf(fw_stats, "%s\t\t\t= %i\n", (setup.language == 0) ? "Nakupene predmety" : "Purchased items  ", setup.all_stats.items_purchased);
	fprintf(fw_stats, "%s\t= %.2f\n", (setup.language == 0) ? "Priemerny utok povodnych bojovnikov" : "Average attack of generated units", setup.all_stats.average_attack_power_generated);
	fprintf(fw_stats, "%s\t= %.2f\n", (setup.language == 0) ? "Priemerna obrana povodnych bojovnikov" : "Average defence of generated units", setup.all_stats.average_defence_power_generated);
	fprintf(fw_stats, "%s\t\t= %.2f\n\n\n", (setup.language == 0) ? "Priemerny pomer utok/obrana" : "Average attack/defence rate", setup.all_stats.average_attack_defence_rate_generated);
	
	fprintf(fw_stats, "%s%s\n", (setup.language == 0) ? "Statistiky hraca " : "Stats of player ", player_1.name);
	fprintf(fw_stats, "====================================================================================================================\n");
	fprintf(fw_stats, "%s\t\t= %i\n", (setup.language == 0) ? "Pocet zostavajucich jednotiek" : "Number of remaining units", setup.all_stats.units_left_p1);
	fprintf(fw_stats, "%s\t\t\t\t= %s\n", (setup.language == 0) ? "Vyvolany demon" : "Summoned demon", setup.all_stats.summoned_demon_p1);
	fprintf(fw_stats, "%s\t\t\t= %s\n", (setup.language == 0) ? "Vymena bojovnikov" : "Swap of the units", setup.all_stats.swap_used_p1);
	fprintf(fw_stats, "%s\t\t\t\t= %i\n", (setup.language == 0) ? "Utratene duse" : "Souls spent", setup.all_stats.souls_spent_p1);
	fprintf(fw_stats, "%s\t\t\t= %i\n", (setup.language == 0) ? "Nakupene predmety" : "Purchased items  ", setup.all_stats.items_purchased_p1);
	fprintf(fw_stats, "%s\t\t\t\t= %i\n", (setup.language == 0) ? "Pocet zasahov" : "Number of hits", setup.all_stats.hits_p1);
	fprintf(fw_stats, "%s\t\t\t= %i\n", (setup.language == 0) ? "Pocet nezasahov v hre" : "Number of non-hits", setup.all_stats.non_hits_p1);
	fprintf(fw_stats, "%s\t\t\t\t= %.2f%%\n", (setup.language == 0) ? "Sanca na zasah" : "The hit rate", setup.all_stats.hit_rate_p1);
	fprintf(fw_stats, "%s\t= %.2f\n", (setup.language == 0) ? "Priemerny utok povodnych bojovnikov" : "Average attack of generated units", setup.all_stats.average_attack_power_p1_generated);
	fprintf(fw_stats, "%s\t= %.2f\n", (setup.language == 0) ? "Priemerna obrana povodnych bojovnikov" : "Average defence of generated units", setup.all_stats.average_defence_power_p1_generated);
	fprintf(fw_stats, "%s\t\t= %.2f\n\n\n", (setup.language == 0) ? "Priemerny pomer utok/obrana" : "Average attack/defence rate", setup.all_stats.average_attack_defence_rate_p1_generated);
	
	fprintf(fw_stats, "%s%s\n", (setup.language == 0) ? "Statistiky hraca " : "Stats of player ", player_2.name);
	fprintf(fw_stats, "====================================================================================================================\n");
	fprintf(fw_stats, "%s\t\t= %i\n", (setup.language == 0) ? "Pocet zostavajucich jednotiek" : "Number of remaining units", setup.all_stats.units_left_p2);
	fprintf(fw_stats, "%s\t\t\t\t= %s\n", (setup.language == 0) ? "Vyvolany demon" : "Summoned demon", setup.all_stats.summoned_demon_p2);
	fprintf(fw_stats, "%s\t\t\t= %s\n", (setup.language == 0) ? "Vymena bojovnikov" : "Swap of the units", setup.all_stats.swap_used_p2);
	fprintf(fw_stats, "%s\t\t\t\t= %i\n", (setup.language == 0) ? "Utratene duse" : "Souls spent", setup.all_stats.souls_spent_p2);
	fprintf(fw_stats, "%s\t\t\t= %i\n", (setup.language == 0) ? "Nakupene predmety" : "Purchased items  ", setup.all_stats.items_purchased_p2);
	fprintf(fw_stats, "%s\t\t\t\t= %i\n", (setup.language == 0) ? "Pocet zasahov" : "Number of hits", setup.all_stats.hits_p2);
	fprintf(fw_stats, "%s\t\t\t= %i\n", (setup.language == 0) ? "Pocet nezasahov v hre" : "Number of non-hits", setup.all_stats.non_hits_p2);
	fprintf(fw_stats, "%s\t\t\t\t= %.2f%%\n", (setup.language == 0) ? "Sanca na zasah" : "The hit rate", setup.all_stats.hit_rate_p2);
	fprintf(fw_stats, "%s\t= %.2f\n", (setup.language == 0) ? "Priemerny utok povodnych bojovnikov" : "Average attack of generated units", setup.all_stats.average_attack_power_p2_generated);
	fprintf(fw_stats, "%s\t= %.2f\n", (setup.language == 0) ? "Priemerna obrana povodnych bojovnikov" : "Average defence of generated units", setup.all_stats.average_defence_power_p2_generated);
	fprintf(fw_stats, "%s\t\t= %.2f\n\n\n", (setup.language == 0) ? "Priemerny pomer utok/obrana" : "Average attack/defence rate", setup.all_stats.average_attack_defence_rate_p2_generated);
	
	if(fclose(fw_stats) == EOF){
		gotoxy(22, 14);
		printf("%s", (setup.language == 0) ? "Chyba pri zatvarani suboru, zapis je otazny!" : "Error while closing file, saving data is questionable!");
		LONG_PAUSE();
		CLEAR_SCREEN();
		SHORT_PAUSE();
		
		basic_setup(setup);
	}
	
	gotoxy(40, 14);
	printf("%s", (setup.language == 0) ? "Data boli zapisane!" : "Data saved!");
	PAUSE();
	CLEAR_SCREEN();
	
	mciSendString("close main_music", NULL, 0, 0);

	SHORT_PAUSE();
	
	basic_setup(setup); // anulacia dat z predchadzajucej hry
}