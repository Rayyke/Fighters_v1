/*
special_abilities.c

Bojovnici 1.1 BETA
==================

Peter Cizmar 7.4.2016
*/

#include "ultimate_header.h"

extern void player_1_special_abilities(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup){
	char input;
	register int i;
	int dead_counter = 0; //pouzivam pri kuzlach
	
	show_player_1_units_status(player_1, setup);
	
	printf("\n%s\t%i\n", (setup.language == 0) ? "Aktualny stav tvojich dusi:" : "Current status of your souls: ", player_1.souls);
																   							   														   
	if(player_1.demon_available == 1)
		printf("%s\n", (setup.language == 0) ? "Moznost privolat demona:\tANO" : "Able to summon the demon:\tYES");
	else if(player_1.demon_available == 0)
		printf("%s\n", (setup.language == 0) ? "Moznost privolat demona:\tNIE" : "Able of summon the demon:\tNO");
		
	if(player_2.swap_available == 1)
		printf("%s", (setup.language == 0) ? "Moznost vymenit bojovnikov:\tANO" : "Able to swap your units:\tYES");
	else if(player_2.swap_available == 0)
		printf("%s", (setup.language == 0) ? "Moznost vymenit bojovnikov:\tNIE" : "Able to swap your units:\tNO");	
			
	printf("\n\n%s\n\n%s\t[%s]\n%s\t[%s]\n%s\t\t[n]", (setup.language == 0) ? "Zelas si privolat demona alebo pouzit duse? " : 
																		"Do you want to summon demon or use some souls?",																																		   
												  	  (setup.language == 0) ? "Vyvolat demona" : "Summon demon", 
												  	  (setup.language == 0) ? "v" : "s", 
												  	  (setup.language == 0) ? "Pouzit duse" : "Use souls",
												  	  (setup.language == 0) ? "p" : "u",
												  	  (setup.language == 0) ? "Nic" : "Nothing");
											
	input = _getch();											
	
	//pokym zly vstup, alebo nemozno vyvolat demona a hrac to skusa
	while((input != ((setup.language == 0) ? 'v' : 's') && input != ((setup.language == 0) ? 'p' : 'u') && input != 'n') || 
		  (player_1.demon_available == 0 && input == ((setup.language == 0) ? 'v' : 's'))){
		printf("\n\n%s\a", (setup.language == 0) ? "Neplatny vstup, alebo privolavanie demona je nedostupne!" : "Wrong input, or the demon summoning is unavailable!");
		input = _getch();
	}
		
	CLEAR_SCREEN();
	
	if(input == ((setup.language == 0) ? 'v' : 's')){
		if(player_1.alive_units == 1){
			CLEAR_SCREEN();
			gotoxy(27, 14);	
			printf("%s", (setup.language == 0) ? "Mas posledneho bojovnika, obeta nie je mozna!" : "You have last unit, sacrifice is not possible!");
			LONG_PAUSE();
			CLEAR_SCREEN();
			SHORT_PAUSE();
			
			player_1_special_abilities(player_1, player_2, setup);	
		}
		
		player_1.achievments.no_bonus_baby = 0;
		
		printf("%s\n\n", (setup.language == 0) ? "Rozhodol si sa vyvolat demona! Jeden tvoj vybrany bojovnik bude obetovany!" : "You decided to summon demon! One of your units will be sacrificed!");
		printf("%s\n\n%s\t[%s]\n%s\t[n]", (setup.language == 0) ? "Chces naozaj pokracovat?" : "Do you want to continue?",
										  (setup.language == 0) ? "Ano" : "Yes",
										  (setup.language == 0) ? "a" : "y",
										  (setup.language == 0) ? "Nie" : "No");
			
		input = _getch();										
		
		while(input != ((setup.language == 0) ? 'a' : 'y') && input != 'n'){
			ERR_MSG;
			input = _getch();
		}
			
		CLEAR_SCREEN();	
	
		if(input == ((setup.language == 0) ? 'a' : 'y')){
			printf("%s\n\n", (setup.language == 0) ? "Ktoreho bojovnika chces obetovat?" : "Which unit do you want to sacrifice?");
				
			show_player_1_units_selection(player_1, setup);
				
			input = _getch();
				
			while(input - ASCII_NUM_VAL < 0 || input - ASCII_NUM_VAL > 4 || player_1.army[input - ASCII_NUM_VAL].health_pool == 0){
				ERR_MSG_OR_DEAD;
				input = _getch();
			}
			
			CLEAR_SCREEN();
				
			player_1.army[input - ASCII_NUM_VAL].health_pool = 0; // SACRIFICE!
			player_1.alive_units--;
			
			gotoxy(34, 14);	
			printf("%s%s", player_1.army[input - ASCII_NUM_VAL].name, (setup.language == 0) ? " bol obetovany demonom!" : " was sacrificed to demons!");
			setup.all_stats.summoned_demons++;

			if(setup.cd_rom_eject_allowed == 1) {
				mciSendString("open CDAudio", NULL, 0, NULL);            // otvor pristup k zariadeniu
				mciSendString("set CDAudio door open", NULL, 0, NULL);   // otvor dvierka
				mciSendString("close CDAudio", NULL, 0, NULL);
			}

			strcpy(setup.all_stats.summoned_demon_p1, (setup.language == 0) ? "ANO" : "YES");
			
			PAUSE();
			CLEAR_SCREEN();
			SHORT_PAUSE();
			
			printf("%s\n\n", (setup.language == 0) ? "Ktoreho demona chces privolat?" : "Which demon do you want to summon?");
				
			printf("Hellriser   [h] %s\n", (setup.language == 0) ? "--> znizi obranu vsetkych nepriatelskych jednotiek o 1 (nemoze byt menej ako 1)" : 
																   "--> decrease the defence of enemy units by 1 (can not be less than 1)");
			printf("Rustkeeper  [r] %s", (setup.language == 0) ? "--> znizi utok vsetkych nepriatelskych jednotiek o 1 (nemoze byt menej ako 1)" : 
																 "--> decrease the attack of enemy units by 1 (can not be less than 1)");
				
			input = _getch();
				
			while(input != 'h' && input != 'r'){
				ERR_MSG;
				input = _getch();
			}
			
			CLEAR_SCREEN();
			demon_summoning();
			SHORT_PAUSE();
			
			gotoxy(8, 14);	
				
			if(input == 'h'){
				for(i = 0; i < MAX_UNITS_TOTAL/2; i++){
					if(player_2.army[i].defence > 1)
						player_2.army[i].defence -= 1;
				}
					
				printf("%s", (setup.language == 0) ? "Nepriatelskym bojovnikom bola znizena obrana o 1 (v pripade ze obrana bola vyssia ako 1)!" : 
													 "The defence of enemy units was decreased by 1 (in case the defence was higher than 1)!");	
			}
			else if(input == 'r'){
				for(i = 0; i < MAX_UNITS_TOTAL/2; i++){
					if(player_2.army[i].attack > 1)
						player_2.army[i].attack -= 1;
				}
					
				printf("%s", (setup.language == 0) ? "Nepriatelskym bojovnikom bol znizeny utok o 1 (v pripade ze utok bol vyssi ako 1)!" : 
													 "The attack of enemy units was decreased by 1 (in case the attack was higher than 1)!");
			}
			
			MORE_LONGER_PAUSE();
			CLEAR_SCREEN();
			SHORT_PAUSE();
				
			player_1.demon_available = 0;
			player_1.achievments.demon_summoner = 1;		
		}
				
		else if(input == 'n')
			player_1_special_abilities(player_1, player_2, setup);
	}
		
	else if(input == ((setup.language == 0) ? 'p' : 'u')){
		player_1.achievments.no_bonus_baby = 0;
		
		printf("%s%i\n\n", (setup.language == 0) ? "Tvoj aktualny stav dusi je ... " : "Your current amount of the souls is ... ", player_1.souls);
		printf("%s\n\n", (setup.language == 0) ? "Co si prajes kupit?" : "What do you want to purchase?");	
		
		if(setup.language == 0){
			printf("\tNazov bonusu\t   Cena   Bonus\n");
			printf("===================================================================================================="); //100
			printf("[1]\tElixir sily\t   1\t   +1 k vlastnostiam svojmu konkretnemu bojovnikovi\n");
			printf("[2]\tElixir zdravia\t   1\t   +2 zivoty svojmu konkretnemu bojovnikovi (max. 5 zivotov)\n");
			printf("[3]\tMagicka puska\t   1\t   -2 zivot konkretnemu nepriatelskemu bojovnikovi\n");
			printf("[4]\tDazd dusi\t   2\t   -1 zivot vsetkym nepriatelskym bojovnikom\n");
			printf("[5]\tShamanizmus\t   2\t   +1 zivot vsetkym tvojim bojovnikom\n");
			printf("[6]\tBojove srdce\t   3\t   +5 utok vsetkym tvojim bojovnikom\n");
			printf("[7]\tSrdce odvahy\t   3\t   +5 obrana vsetkym tvojim bojovnikom\n");
			printf("[8]\tWillbreaker (mec)  4\t   +10 utok a +8 zivotov pre svojho konkretneho bojovnika\n");
			printf("[9]\tTitanskin (stit)   4\t   +10 obrana a +8 zivotov pre svojho konkretneho bojovnika\n\n");
			
			printf("[0]\tNic");
		}							   
		else if(setup.language == 1){
			printf("\tName of bonus\t     Price   Bonus\n");
			printf("===================================================================================================="); //100
			printf("[1]\tPotion of power\t     1\t     +1 to all stats of your certain unit\n");
			printf("[2]\tPotion of healing    1\t     +2 lives your certain unit (max. 5 lives)\n");
			printf("[3]\tMagic rifle\t     1\t     -2 lives to certain enemy unit\n");
			printf("[4]\tRain of souls\t     2\t     -1 life to all enemy units\n");
			printf("[5]\tShamanism\t     2\t     +1 life to all your units\n");
			printf("[6]\tWar heart\t     3\t     +5 attack to all your units\n");
			printf("[7]\tHeart of courage     3\t     +5 defence to all your units\n");
			printf("[8]\tWillbreaker (sword)  4\t     +10 attack and +8 lives to your certain unit\n");
			printf("[9]\tTitanskin (shield)   4\t     +10 defence and +8 lives to your certain unit\n\n");
			
			printf("[0]\tNothing");
		}
		
		input = _getch();
		
		while(input - ASCII_NUM_VAL < 0 || input - ASCII_NUM_VAL > 9 || (input - ASCII_NUM_VAL == 1 && player_1.souls < 1) ||
																		(input - ASCII_NUM_VAL == 2 && player_1.souls < 1) ||
																		(input - ASCII_NUM_VAL == 3 && player_1.souls < 1) ||
																		(input - ASCII_NUM_VAL == 4 && player_1.souls < 2) ||
																		(input - ASCII_NUM_VAL == 5 && player_1.souls < 2) ||
																		(input - ASCII_NUM_VAL == 6 && player_1.souls < 3) ||
																		(input - ASCII_NUM_VAL == 7 && player_1.souls < 3) ||
																		(input - ASCII_NUM_VAL == 8 && player_1.souls < 4) ||
																		(input - ASCII_NUM_VAL == 9 && player_1.souls < 4)){
			printf("\n\n%s(%i)!\a", (setup.language == 0) ? "Neplatny vstup, alebo nedostatocny pocet dusi " : 
													  		"Wrong input or you do not have enaugh souls ", player_1.souls);
			input = _getch();
		}	
		
		CLEAR_SCREEN();			
		
		switch(input - ASCII_NUM_VAL){
			case 0:
				player_1_special_abilities(player_1, player_2, setup);
				break;
				
			case 1:
				printf("%s\n\n", (setup.language == 0) ? "Vyber si bojovnika, ktory dostane +1 do vsetkych vlastnosti!" : 
												   		 "Choose a unit, that will receive +1 to all his stats!");
				
				show_player_1_units_selection(player_1, setup);
				
				input = _getch();
				
				while(input - ASCII_NUM_VAL < 0 || input - ASCII_NUM_VAL > 4 || player_1.army[input - ASCII_NUM_VAL].health_pool == 0){
					ERR_MSG_OR_DEAD;
					input = _getch();
				}
				
				player_1.army[input - ASCII_NUM_VAL].health_pool += 1;
				player_1.army[input - ASCII_NUM_VAL].attack += 1;
				player_1.army[input - ASCII_NUM_VAL].defence += 1;
				player_1.souls -= 1;
				setup.all_stats.souls_spent_p1 += 1;
				setup.all_stats.items_purchased_p1++;
				player_1.achievments.alchemist++;
				
				CLEAR_SCREEN();	
				potion_of_power();
				system("COLOR 0F");	
				
				SHORT_PAUSE();
				
				gotoxy(25, 14);
				
				printf("%s%s", player_1.army[input - ASCII_NUM_VAL].name, (setup.language == 0) ? " ziskal bonus +1 ku vsetkym vlastnostiam!" : 
																								  " obtained the bonus +1 to all stats!");
				
				LONGER_PAUSE();
				CLEAR_SCREEN();
				SHORT_PAUSE();
				break;
			
			case 2:
				printf("%s\n\n", (setup.language == 0) ? "Vyber si bojovnika, ktory dostane 2 zivoty!" : 
												   		 "Choose a unit that will receive 2 lives!");
				
				show_player_1_units_selection(player_1, setup);
				
				input = _getch();
				
				while(input - ASCII_NUM_VAL < 0 || input - ASCII_NUM_VAL > 4 || player_1.army[input - ASCII_NUM_VAL].health_pool == 0){
					ERR_MSG_OR_DEAD;
					input = _getch();
				}
				
				CLEAR_SCREEN();	
				
				if(player_1.army[input - ASCII_NUM_VAL].health_pool >= 5){
					gotoxy(12, 14);
					printf("%s", (setup.language == 0) ? "Bojovnik ma 5 a viacej zivota. Nie je co uzdravovat, dusa nebola odobrana!" :
													   	 "Unit has 5 or more lives. There is nothing to heal, soul was not spent!");
					MORE_LONGER_PAUSE();
					CLEAR_SCREEN();
					SHORT_PAUSE();
				}		
				else if(player_1.army[input - ASCII_NUM_VAL].health_pool == 4){
					potion_of_healing();
					system("COLOR 0F");	
					
					SHORT_PAUSE();
					
					player_1.army[input - ASCII_NUM_VAL].health_pool += 1;
					gotoxy(38, 14);
					printf("%s%s", player_1.army[input - ASCII_NUM_VAL].name, (setup.language == 0) ? " ziskal 1 zivot!" : " obtained 1 life!");
					
					LONGER_PAUSE();
					CLEAR_SCREEN();
					SHORT_PAUSE();
					
					player_1.souls -= 1;
					setup.all_stats.souls_spent_p1 += 1;
					setup.all_stats.items_purchased_p1++;
					player_1.achievments.alchemist++;
				}
				else{
					potion_of_healing();
					system("COLOR 0F");	
					
					SHORT_PAUSE();
					
					player_1.army[input - ASCII_NUM_VAL].health_pool += 2;
					gotoxy(38, 14);
					printf("%s%s", player_1.army[input - ASCII_NUM_VAL].name, (setup.language == 0) ? " ziskal 2 zivoty!" : " obtained 2 lives!");
					
					LONGER_PAUSE();
					CLEAR_SCREEN();
					SHORT_PAUSE();
					
					player_1.souls -= 1;
					setup.all_stats.souls_spent_p1 += 1;
					setup.all_stats.items_purchased_p1++;
					player_1.achievments.alchemist++;
				}
				break;	
				
			case 3:
				printf("%s\n\n", (setup.language == 0) ? "Vyber si nepriatelsku jednotku, ktoru chces zasiahnut!" :
											   	   		 "Choose the enemy unit that you want to hit!");
											   
				show_player_2_units_selection(player_2, setup);	
				
				input = _getch();
				
				while(input - ASCII_NUM_VAL < 0 || input - ASCII_NUM_VAL > 4 || player_2.army[input - ASCII_NUM_VAL].health_pool == 0){
					ERR_MSG_OR_DEAD;
					input = _getch();
				}
				
				CLEAR_SCREEN();	
				magic_rifle();
				system("COLOR 0F");	
				
				SHORT_PAUSE();
				
				if(player_2.army[input - ASCII_NUM_VAL].health_pool == 1){
					player_2.army[input - ASCII_NUM_VAL].health_pool -= 1;
					player_2.alive_units--;
					player_1.achievments.sniper++;
					player_1.achievments.killing_spree[1]++;
					player_2.achievments.killing_spree[1] = 0;
					
					gotoxy(32, 14);
					printf("%s%s", player_2.army[input - ASCII_NUM_VAL].name, (setup.language == 0) ? " stratil 1 zivot a zomrel!" : 
																									  " lost 1 life and died!");

					if (setup.cd_rom_eject_allowed == 1) {
						mciSendString("open CDAudio", NULL, 0, NULL);            // otvor pristup k zariadeniu
						mciSendString("set CDAudio door open", NULL, 0, NULL);   // otvor dvierka
						mciSendString("close CDAudio", NULL, 0, NULL);
					}
																									  
					LONGER_PAUSE();
					CLEAR_SCREEN();
					SHORT_PAUSE();																				  
				}	
				else{
					player_2.army[input - ASCII_NUM_VAL].health_pool -= 2;
					
					if(player_2.army[input - ASCII_NUM_VAL].health_pool == 0){
						player_1.achievments.sniper++;
						player_2.alive_units--;
						
						gotoxy(32, 14);
						printf("%s%s", player_2.army[input - ASCII_NUM_VAL].name, (setup.language == 0) ? " stratil 2 zivoty a zomrel!" : 
																										  " lost 2 lives and died!");
																										  
						player_1.achievments.killing_spree[1]++;
						player_2.achievments.killing_spree[1] = 0;	

						if (setup.cd_rom_eject_allowed == 1) {
							mciSendString("open CDAudio", NULL, 0, NULL);            // otvor pristup k zariadeniu
							mciSendString("set CDAudio door open", NULL, 0, NULL);   // otvor dvierka
							mciSendString("close CDAudio", NULL, 0, NULL);
						}
						
						LONGER_PAUSE();
						CLEAR_SCREEN();
						SHORT_PAUSE();																		  
					}	
					else{
						gotoxy(32, 14);
						printf("%s%s", player_2.army[input - ASCII_NUM_VAL].name, (setup.language == 0) ? " stratil 2 zivoty ale prezil!" : 
																										  " lost 2 lives but survived!");
						
						LONGER_PAUSE();
						CLEAR_SCREEN();
						SHORT_PAUSE();
					}																					  
				}
																								
				player_1.souls -= 1;
				setup.all_stats.souls_spent_p1 += 1;
				setup.all_stats.items_purchased_p1++;
				player_1.achievments.soldier++;																				
				
				check_winner(player_1, player_2, setup);			   
				break;
				
			case 4:
				for(i = 0; i < 5; i++){
					if(player_2.army[i].health_pool > 1)
						player_2.army[i].health_pool -= 1;
					else if(player_2.army[i].health_pool == 1){
						player_2.army[i].health_pool -= 1;
						dead_counter++;
						player_2.alive_units--;
						player_1.achievments.mass_destruction++;
						player_1.achievments.killing_spree[1]++;
						player_2.achievments.killing_spree[1] = 0;
					}
				}
					
				player_1.souls -= 2;
				setup.all_stats.souls_spent_p1 += 2;
				setup.all_stats.items_purchased_p1++;	
				
				CLEAR_SCREEN();	
				rain_of_souls();
				system("COLOR 0F");	
				
				SHORT_PAUSE();
				
				gotoxy(25, 14);
				printf("%s", (setup.language == 0) ? "Vsetky zive nepriatelske jednotky stratili 1 zivot!" : 
											     	 "All alive enemy units lost 1 life!");
											     	 
				LONGER_PAUSE();
				CLEAR_SCREEN();
				SHORT_PAUSE();							     	 
											   
				if(dead_counter != 0){
					gotoxy(38, 14);	
					printf("%s%i%s", (setup.language == 0) ? "Zabil si " : "You killed ", dead_counter, 
									 	 (setup.language == 0) ? " bojovnika/-ov!" : " enemy units!"); 

					if (setup.cd_rom_eject_allowed == 1) {
						mciSendString("open CDAudio", NULL, 0, NULL);            // otvor pristup k zariadeniu
						mciSendString("set CDAudio door open", NULL, 0, NULL);   // otvor dvierka
						mciSendString("close CDAudio", NULL, 0, NULL);
					}
										  
					LONGER_PAUSE();
					CLEAR_SCREEN();
					SHORT_PAUSE();	
				}
										 	 
				check_winner(player_1, player_2, setup);
				break;
				
			case 5:
				for(i = 0; i < 5; i++){
					if(player_1.army[i].health_pool != 0) //dolezite na neozivovanie jednotiek
						player_1.army[i].health_pool += 1; 
				}
				
				player_1.souls -= 2;
				setup.all_stats.souls_spent_p1 += 2;
				setup.all_stats.items_purchased_p1++;	
				
				CLEAR_SCREEN();	
				shamanism();
				system("COLOR 0F");	
				
				SHORT_PAUSE();
				
				gotoxy(25, 14);	
				printf("%s", (setup.language == 0) ? "Vsetky tvoje zive jednotky ziskali bonus 1 zivot!" : 
							  			       		 "All your alive units obtained bonus 1 life!");
														  
				LONGER_PAUSE();
				CLEAR_SCREEN();
				SHORT_PAUSE();										  			  			       
				break;
				
			case 6:
				for(i = 0; i < 5; i++){
					if(player_1.army[i].health_pool != 0)
						player_1.army[i].attack += 5; 
				}
				
				player_1.souls -= 3;
				setup.all_stats.souls_spent_p1 += 3;
				setup.all_stats.items_purchased_p1++;
				player_1.achievments.heart_warming++;	
				
				CLEAR_SCREEN();	
				war_heart();
				system("COLOR 0F");	
				
				SHORT_PAUSE();
				
				gotoxy(25, 14);	
				printf("%s", (setup.language == 0) ? "Vsetky tvoje zive jednotky ziskali +5 utok!" : 
											   		 "All your alive units obtained +5 attack!");
											   		 
				LONGER_PAUSE();
				CLEAR_SCREEN();
				SHORT_PAUSE();							   		 
				break;
				
			case 7:
				for(i = 0; i < 5; i++){
					if(player_1.army[i].health_pool != 0)
						player_1.army[i].defence += 5; 
				}
				
				player_1.souls -= 3;
				setup.all_stats.souls_spent_p1 += 3;
				setup.all_stats.items_purchased_p1++;
				player_1.achievments.heart_warming++;	
				
				CLEAR_SCREEN();	
				heart_of_courage();
				system("COLOR 0F");	
				
				SHORT_PAUSE();
				
				gotoxy(25, 14);	
				printf("%s", (setup.language == 0) ? "Vsetky tvoje zive jednotky ziskali +5 obranu!" : 
											   		 "All your alive units obtained +5 defence!");
											   		 
				LONGER_PAUSE();
				CLEAR_SCREEN();
				SHORT_PAUSE();							   		 
				break;
				
			case 8:
				printf("%s\n\n", (setup.language == 0) ? "Vyber si bojovnika, ktory ziska +10 utok a bonus 8 zivotov!" : 
												   		 "Choose a unit that will obtain +10 attack and bonus 8 lives!");
				
				show_player_1_units_selection(player_1, setup);
				
				input = _getch();
				
				while(input - ASCII_NUM_VAL < 0 || input - ASCII_NUM_VAL > 4 || player_1.army[input - ASCII_NUM_VAL].health_pool == 0){
					ERR_MSG_OR_DEAD;
					input = _getch();
				}
				
				player_1.army[input - ASCII_NUM_VAL].health_pool += 8;
				player_1.army[input - ASCII_NUM_VAL].attack += 10;
				player_1.souls -= 4;
				setup.all_stats.souls_spent_p1 += 4;
				setup.all_stats.items_purchased_p1++;
				player_1.achievments.legendary_possession++;
				
				CLEAR_SCREEN();	
				willbreaker();
				system("COLOR 0F");	
				
				SHORT_PAUSE();
				
				gotoxy(36, 14);	
				printf("%s%s", player_1.army[input - ASCII_NUM_VAL].name, (setup.language == 0) ? " ziskal Willbreaker!" : 
																								  " obtained Willbreaker!");
																								  
				LONGER_PAUSE();
				CLEAR_SCREEN();
				SHORT_PAUSE();																				  
				break;
				
			case 9:
				printf("%s\n\n", (setup.language == 0) ? "Vyber si bojovnika, ktory ziska +10 obranu a bonus 8 zivotov!" : 
												   		 "Choose a unit that will obtain +10 defence and bonus 8 lives!");
				
				show_player_1_units_selection(player_1, setup);
				
				input = _getch();
				
				while(input - ASCII_NUM_VAL < 0 || input - ASCII_NUM_VAL > 4 || player_1.army[input - ASCII_NUM_VAL].health_pool == 0){
					ERR_MSG_OR_DEAD;
					input = _getch();
				}
				
				player_1.army[input - ASCII_NUM_VAL].health_pool += 8;
				player_1.army[input - ASCII_NUM_VAL].defence += 10;
				player_1.souls -= 4;
				setup.all_stats.souls_spent_p1 += 4;
				setup.all_stats.items_purchased_p1++;
				player_1.achievments.legendary_possession++;
				
				CLEAR_SCREEN();	
				titanskin();
				system("COLOR 0F");	
				
				SHORT_PAUSE();
				
				gotoxy(35, 14);	
				printf("%s%s", player_1.army[input - ASCII_NUM_VAL].name, (setup.language == 0) ? " ziskal Titanskin!" : 
																								  " obtained Titanskin!");
																								  
				LONGER_PAUSE();
				CLEAR_SCREEN();
				SHORT_PAUSE();																				  
				break;
				
			default:
				break;
		}					 							   						   
	}	
	else if(input == 'n') // nic or nothing
		play(player_1, player_2, setup); // jedina moznost ako sa dostat prec z menu pre magicke schopnosti, inac rekurzia (warning vo Visualku)
	
	player_1_special_abilities(player_1, player_2, setup); //rekurzia dokial vyslovene nepoviem poviem ze NECHCEM nic	
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extern void player_2_special_abilities(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup){
	char input;
	register int i;
	int dead_counter = 0; //pouzivam pri kuzlach
	
	CLEAR_SCREEN();
	
	show_player_2_units_status(player_2, setup);
	
	printf("\n%s\t%i\n", (setup.language == 0) ? "Aktualny stav tvojich dusi:" : "Current status of your souls: ", player_2.souls);
																   							   														   
	if(player_2.demon_available == 1)
		printf("%s\n", (setup.language == 0) ? "Moznost privolat demona:\tANO" : "Able to summon the demon:\tYES");
	else if(player_2.demon_available == 0)
		printf("%s\n", (setup.language == 0) ? "Moznost privolat demona:\tNIE" : "Able of summon the demon:\tNO");
		
	if(player_2.swap_available == 1)
		printf("%s", (setup.language == 0) ? "Moznost vymenit bojovnikov:\tANO" : "Able to swap your units:\tYES");
	else if(player_2.swap_available == 0)
		printf("%s", (setup.language == 0) ? "Moznost vymenit bojovnikov:\tNIE" : "Able to swap your units:\tNO");
			
	printf("\n\n%s\n\n%s\t[%s]\n%s\t[%s]\n%s\t\t[n]", (setup.language == 0) ? "Zelas si privolat demona alebo pouzit duse? " : 
																		"Do you want to summon demon or use some souls?",																																		   
												  	  (setup.language == 0) ? "Vyvolat demona" : "Summon demon", 
												  	  (setup.language == 0) ? "v" : "s", 
												  	  (setup.language == 0) ? "Pouzit duse" : "Use souls",
												  	  (setup.language == 0) ? "p" : "u",
												  	  (setup.language == 0) ? "Nic" : "Nothing");
											
	input = _getch();											
	
	//pokym zly vstup, alebo nemozno vyvolat demona a hrac to skusa
	while((input != ((setup.language == 0) ? 'v' : 's') && input != ((setup.language == 0) ? 'p' : 'u') && input != 'n') || 
	 	  (player_2.demon_available == 0 && input == ((setup.language == 0) ? 'v' : 's'))){
		printf("\n\n%s\a", (setup.language == 0) ? "Neplatny vstup, alebo privolavanie demona je nedostupne!" : "Wrong input, or the demon summoning is unavailable!");
		input = _getch();
	}
		
	CLEAR_SCREEN();
	
	if(input == ((setup.language == 0) ? 'v' : 's')){
		if(player_2.alive_units == 1){
			CLEAR_SCREEN();
			gotoxy(27, 14);	
			printf("%s", (setup.language == 0) ? "Mas posledneho bojovnika, obeta nie je mozna!" : "You have last unit, sacrifice is not possible!");
			LONG_PAUSE();
			CLEAR_SCREEN();
			SHORT_PAUSE();
			
			player_2_special_abilities(player_1, player_2, setup);	
		}
		
		player_2.achievments.no_bonus_baby = 0;
		
		printf("%s\n\n", (setup.language == 0) ? "Rozhodol si sa vyvolat demona! Jeden tvoj vybrany bojovnik bude obetovany!" : 
												 "You decided to summon demon! One of your units will be sacrificed!");
		printf("%s\n\n%s\t[%s]\n%s\t[n]", (setup.language == 0) ? "Chces naozaj pokracovat?" : "Do you want to continue?",
										  (setup.language == 0) ? "Ano" : "Yes",
										  (setup.language == 0) ? "a" : "y",
										  (setup.language == 0) ? "Nie" : "No");
			
		input = _getch();										
		
		while(input != ((setup.language == 0) ? 'a' : 'y') && input != 'n'){
			ERR_MSG;
			input = _getch();
		}
			
		CLEAR_SCREEN();	
	
		if(input == ((setup.language == 0) ? 'a' : 'y')){
			printf("%s\n\n", (setup.language == 0) ? "Ktoreho bojovnika chces obetovat?" : "Which unit do you want to sacrifice?");
				
			show_player_2_units_selection(player_2, setup);
				
			input = _getch();
				
			while(input - ASCII_NUM_VAL < 0 || input - ASCII_NUM_VAL > 4 || player_2.army[input - ASCII_NUM_VAL].health_pool == 0){
				ERR_MSG_OR_DEAD;
				input = _getch();
			}
			
			CLEAR_SCREEN();
				
			player_2.army[input - ASCII_NUM_VAL].health_pool = 0; // SACRIFICE!
			player_2.alive_units--;
			
			gotoxy(34, 14);		
			printf("%s%s", player_2.army[input - ASCII_NUM_VAL].name, (setup.language == 0) ? " bol obetovany demonom!" : " was sacrificed to demons!");
			setup.all_stats.summoned_demons++;

			if (setup.cd_rom_eject_allowed == 1) {
				mciSendString("open CDAudio", NULL, 0, NULL);            // otvor pristup k zariadeniu
				mciSendString("set CDAudio door open", NULL, 0, NULL);   // otvor dvierka
				mciSendString("close CDAudio", NULL, 0, NULL);
			}

			strcpy(setup.all_stats.summoned_demon_p2, (setup.language == 0) ? "ANO" : "YES");
			
			PAUSE();
			CLEAR_SCREEN();
			SHORT_PAUSE();
				
			printf("%s\n\n", (setup.language == 0) ? "Ktoreho demona chces privolat?" : "Which demon do you want to summon?");
				
			printf("Hellriser   [h] %s\n", (setup.language == 0) ? "--> znizi obranu vsetkych nepriatelskych jednotiek o 1" : "--> decrease the defence of enemy units by 1");
			printf("Rustkeeper  [r] %s", (setup.language == 0) ? "--> znizi utok vsetkych nepriatelskych jednotiek o 1" : "--> decrease the attack of enemy units by 1");
				
			input = _getch();
				
			while(input != 'h' && input != 'r'){
				ERR_MSG;
				input = _getch();
			}
			
			CLEAR_SCREEN();
			demon_summoning();
			SHORT_PAUSE();
			
			gotoxy(8, 14);	
				
			if(input == 'h'){
				for(i = 0; i < MAX_UNITS_TOTAL/2; i++){
					if(player_1.army[i].defence > 1)
						player_1.army[i].defence -= 1;
				}
					
				printf("%s", (setup.language == 0) ? "Nepriatelskym bojovnikom bola znizena obrana o 1 (v pripade ze obrana bola vyssia ako 1)!" : 
													 "The defence of enemy units was decreased by 1 (in case the defence was higher than 1)!");	
			}
			else if(input == 'r'){
				for(i = 0; i < MAX_UNITS_TOTAL/2; i++){
					if(player_1.army[i].attack > 1)
						player_1.army[i].attack -= 1;
				}
					
				printf("%s", (setup.language == 0) ? "Nepriatelskym bojovnikom bol znizeny utok o 1 (v pripade ze utok bol vyssi ako 1)!" : 
													 "The attack of enemy units was decreased by 1 (in case the attack was higher than 1)!");
			}
			
			MORE_LONGER_PAUSE();
			CLEAR_SCREEN();
			SHORT_PAUSE();
				
			player_2.demon_available = 0;
			player_2.achievments.demon_summoner = 1;		
		}
				
		else if(input == 'n')
			player_2_special_abilities(player_1, player_2, setup);
	}
		
	else if(input == ((setup.language == 0) ? 'p' : 'u')){
		player_2.achievments.no_bonus_baby = 0;
		
		printf("%s%i\n\n", (setup.language == 0) ? "Tvoj aktualny stav dusi je ... " : "Your current amount of the souls is ... ",player_2.souls);
		printf("%s\n\n", (setup.language == 0) ? "Co si prajes kupit?" : "What do you want to purchase?");	
		
		if(setup.language == 0){
			printf("\tNazov bonusu\t   Cena   Bonus\n");
			printf("===================================================================================================="); //100
			printf("[1]\tElixir sily\t   1\t   +1 k vlastnostiam svojmu konkretnemu bojovnikovi\n");
			printf("[2]\tElixir zdravia\t   1\t   +2 zivoty svojmu konkretnemu bojovnikovi (max. 5 zivotov)\n");
			printf("[3]\tMagicka puska\t   1\t   -2 zivot konkretnemu nepriatelskemu bojovnikovi\n");
			printf("[4]\tDazd dusi\t   2\t   -1 zivot vsetkym nepriatelskym bojovnikom\n");
			printf("[5]\tShamanizmus\t   2\t   +1 zivot vsetkym tvojim bojovnikom\n");
			printf("[6]\tBojove srdce\t   3\t   +5 utok vsetkym tvojim bojovnikom\n");
			printf("[7]\tSrdce odvahy\t   3\t   +5 obrana vsetkym tvojim bojovnikom\n");
			printf("[8]\tWillbreaker (mec)  4\t   +10 utok a +8 zivotov pre svojho konkretneho bojovnika\n");
			printf("[9]\tTitanskin (stit)   4\t   +10 obrana a +8 zivotov pre svojho konkretneho bojovnika\n\n");
			
			printf("[0]\tNic");
		}							   	
		else if(setup.language == 1){
			printf("\tName of bonus\t     Price   Bonus\n");
			printf("===================================================================================================="); //100
			printf("[1]\tPotion of power\t     1\t     +1 to all stats of your certain unit\n");
			printf("[2]\tPotion of healing    1\t     +2 lives your certain unit (max. 5 lives)\n");
			printf("[3]\tMagic rifle\t     1\t     -2 lives to certain enemy unit\n");
			printf("[4]\tRain of souls\t     2\t     -1 life to all enemy units\n");
			printf("[5]\tShamanism\t     2\t     +1 life to all your units\n");
			printf("[6]\tWar heart\t     3\t     +5 attack to all your units\n");
			printf("[7]\tHeart of courage     3\t     +5 defence to all your units\n");
			printf("[8]\tWillbreaker (sword)  4\t     +10 attack and +8 lives to your certain unit\n");
			printf("[9]\tTitanskin (shield)   4\t     +10 defence and +8 lives to your certain unit\n\n");
			
			printf("[0]\tNothing");
		}
		
		input = _getch();
		
		while(input - ASCII_NUM_VAL < 0 || input - ASCII_NUM_VAL > 9 || (input - ASCII_NUM_VAL == 1 && player_2.souls < 1) ||
																		(input - ASCII_NUM_VAL == 2 && player_2.souls < 1) ||
																		(input - ASCII_NUM_VAL == 3 && player_2.souls < 1) ||
																		(input - ASCII_NUM_VAL == 4 && player_2.souls < 2) ||
																		(input - ASCII_NUM_VAL == 5 && player_2.souls < 2) ||
																		(input - ASCII_NUM_VAL == 6 && player_2.souls < 3) ||
																		(input - ASCII_NUM_VAL == 7 && player_2.souls < 3) ||
																		(input - ASCII_NUM_VAL == 8 && player_2.souls < 4) ||
																		(input - ASCII_NUM_VAL == 9 && player_2.souls < 4)){
			printf("\n\n%s(%i)!\a", (setup.language == 0) ? "Neplatny vstup, alebo nedostatocny pocet dusi " : 
													  		"Wrong input or you do not have enaugh souls ", player_2.souls);
			input = _getch();
		}	
		
		CLEAR_SCREEN();				
		
		switch(input - ASCII_NUM_VAL){
			case 0:
				player_2_special_abilities(player_1, player_2, setup);
				break;
				
			case 1:
				printf("%s\n\n", (setup.language == 0) ? "Vyber si bojovnika, ktory dostane +1 do vsetkych vlastnosti!" : 
												   		 "Choose a unit, that will receive +1 to all his stats!");
				
				show_player_2_units_selection(player_2, setup);
				
				input = _getch();
				
				while(input - ASCII_NUM_VAL < 0 || input - ASCII_NUM_VAL > 4 || player_2.army[input - ASCII_NUM_VAL].health_pool == 0){
					ERR_MSG_OR_DEAD;
					input = _getch();
				}
				
				player_2.army[input - ASCII_NUM_VAL].health_pool += 1;
				player_2.army[input - ASCII_NUM_VAL].attack += 1;
				player_2.army[input - ASCII_NUM_VAL].defence += 1;
				player_2.souls -= 1;
				setup.all_stats.souls_spent_p2 += 1;
				setup.all_stats.items_purchased_p2++;
				player_2.achievments.alchemist++;
				
				CLEAR_SCREEN();	
				potion_of_power();
				system("COLOR 0F");	
				
				SHORT_PAUSE();
				
				gotoxy(25, 14);
				
				printf("%s%s", player_2.army[input - ASCII_NUM_VAL].name, (setup.language == 0) ? " ziskal bonus +1 ku vsetkym vlastnostiam!" : 
																								  " obtained the bonus +1 to all stats!");
				
				LONGER_PAUSE();
				CLEAR_SCREEN();
				SHORT_PAUSE();
				break;
				
			case 2:
				printf("%s\n\n", (setup.language == 0) ? "Vyber si bojovnika, ktory dostane 2 zivoty!" : 
												   		 "Choose a unit that will receive 2 lives!");
				
				show_player_2_units_selection(player_2, setup);
				
				input = _getch();
				
				while(input - ASCII_NUM_VAL < 0 || input - ASCII_NUM_VAL > 4 || player_2.army[input - ASCII_NUM_VAL].health_pool == 0){
					ERR_MSG_OR_DEAD;
					input = _getch();
				}
				
				CLEAR_SCREEN();	
				
				gotoxy(12, 14);
				if(player_2.army[input - ASCII_NUM_VAL].health_pool >= 5){
					printf("%s", (setup.language == 0) ? "Bojovnik ma 5 a viacej zivota. Nie je co uzdravovat, dusa nebola odobrana!" :
												   		 "Unit has 5 or more lives. There is nothing to heal, soul was not spent!");
												   		 
					MORE_LONGER_PAUSE();
					CLEAR_SCREEN();
					SHORT_PAUSE();								   		 	
				}		
				else if(player_2.army[input - ASCII_NUM_VAL].health_pool == 4){
					potion_of_healing();
					system("COLOR 0F");	
					
					SHORT_PAUSE();
					
					gotoxy(38, 14);
					player_2.army[input - ASCII_NUM_VAL].health_pool += 1;
					printf("%s%s", player_2.army[input - ASCII_NUM_VAL].name, (setup.language == 0) ? " ziskal 1 zivot!" : " obtained 1 life!");
					
					LONGER_PAUSE();
					CLEAR_SCREEN();
					
					
					player_2.souls -= 1;
					setup.all_stats.souls_spent_p2 += 1;
					setup.all_stats.items_purchased_p2++;
					player_2.achievments.alchemist++;
				}
				else{
					potion_of_healing();
					system("COLOR 0F");	
					
					SHORT_PAUSE();
					
					gotoxy(38, 14);					
					player_2.army[input - ASCII_NUM_VAL].health_pool += 2;
					printf("%s%s", player_2.army[input - ASCII_NUM_VAL].name, (setup.language == 0) ? " ziskal 2 zivoty!" : " obtained 2 lives!");
					
					LONGER_PAUSE();
					CLEAR_SCREEN();
					SHORT_PAUSE();
					
					player_2.souls -= 1;
					setup.all_stats.souls_spent_p2 += 1;
					setup.all_stats.items_purchased_p2++;
					player_2.achievments.alchemist++;
				}
				break;			
				
			case 3:
				printf("%s\n\n", (setup.language == 0) ? "Vyber si nepriatelsku jednotku, ktoru chces zasiahnut!" :
											   	   		 "Choose the enemy unit that you want to hit!");
											   
				show_player_1_units_selection(player_1, setup);	
				
				input = _getch();
				
				while(input - ASCII_NUM_VAL < 0 || input - ASCII_NUM_VAL > 4 || player_1.army[input - ASCII_NUM_VAL].health_pool == 0){
					ERR_MSG_OR_DEAD;
					input = _getch();
				}
				
				CLEAR_SCREEN();	
				magic_rifle();
				system("COLOR 0F");	
				
				SHORT_PAUSE();
				
				if(player_1.army[input - ASCII_NUM_VAL].health_pool == 1){
					player_1.army[input - ASCII_NUM_VAL].health_pool -= 1;
					player_1.alive_units--;
					player_2.achievments.sniper++;
					player_2.achievments.killing_spree[1]++;
					player_1.achievments.killing_spree[1] = 0;
					
					gotoxy(32, 14);
					printf("%s%s", player_1.army[input - ASCII_NUM_VAL].name, (setup.language == 0) ? " stratil 1 zivot a zomrel!" : 
																									  " lost 1 life and died!");

					if (setup.cd_rom_eject_allowed == 1) {
						mciSendString("open CDAudio", NULL, 0, NULL);            // otvor pristup k zariadeniu
						mciSendString("set CDAudio door open", NULL, 0, NULL);   // otvor dvierka
						mciSendString("close CDAudio", NULL, 0, NULL);
					}
																									  
					LONGER_PAUSE();
					CLEAR_SCREEN();
					SHORT_PAUSE();																				  
				}
					
				else{
					player_1.army[input - ASCII_NUM_VAL].health_pool -= 2;
					
					if(player_1.army[input - ASCII_NUM_VAL].health_pool == 0){
						player_2.achievments.sniper++;
						player_1.alive_units--;
						
						gotoxy(32, 14);
						printf("%s%s", player_1.army[input - ASCII_NUM_VAL].name, (setup.language == 0) ? " stratil 2 zivoty a zomrel!" : 
																										  " lost 2 lives and died!");	

						if (setup.cd_rom_eject_allowed == 1) {
							mciSendString("open CDAudio", NULL, 0, NULL);            // otvor pristup k zariadeniu
							mciSendString("set CDAudio door open", NULL, 0, NULL);   // otvor dvierka
							mciSendString("close CDAudio", NULL, 0, NULL);
						}
																							
						player_2.achievments.killing_spree[1]++;
						player_1.achievments.killing_spree[1] = 0;	
						
						LONGER_PAUSE();
						CLEAR_SCREEN();
						SHORT_PAUSE();																			  
					}	
					else{
						gotoxy(32, 14);
						printf("%s%s", player_1.army[input - ASCII_NUM_VAL].name, (setup.language == 0) ? " stratil 2 zivoty ale prezil!" : 
																										  " lost 2 lives but survived!");
						
						LONGER_PAUSE();
						CLEAR_SCREEN();
						SHORT_PAUSE();
					}		
				}
																								
				player_2.souls -= 1;
				setup.all_stats.souls_spent_p2 += 1;
				setup.all_stats.items_purchased_p2++;
				player_2.achievments.soldier++;																					
				
				check_winner(player_1, player_2, setup);		   
				break;
				
			case 4:
				for(i = 0; i < 5; i++){
					if(player_1.army[i].health_pool > 1)
						player_1.army[i].health_pool -= 1;
					else if(player_1.army[i].health_pool == 1){
						player_1.army[i].health_pool -= 1;
						dead_counter++;
						player_1.alive_units--;
						player_2.achievments.mass_destruction++;
						player_2.achievments.killing_spree[1]++;
						player_1.achievments.killing_spree[1] = 0;
					}	
				}
					
				player_2.souls -= 2;
				setup.all_stats.souls_spent_p2 += 2;
				setup.all_stats.items_purchased_p2++;	
				
				CLEAR_SCREEN();	
				rain_of_souls();
				system("COLOR 0F");	
				
				SHORT_PAUSE();
				
				gotoxy(25, 14);
				printf("%s", (setup.language == 0) ? "Vsetky zive nepriatelske jednotky stratili 1 zivot!" : 
											     	 "All alive enemy units lost 1 life!");
											     	 
				LONGER_PAUSE();
				CLEAR_SCREEN();
				SHORT_PAUSE();							     	 
								   
				if(dead_counter != 0){
					gotoxy(38, 14);	
					printf("%s%i%s", (setup.language == 0) ? "Zabil si " : "You killed ", dead_counter, (setup.language == 0) ? 
													   		 	 " bojovnika/-ov!" : " enemy units!");

					if (setup.cd_rom_eject_allowed == 1) {
						mciSendString("open CDAudio", NULL, 0, NULL);            // otvor pristup k zariadeniu
						mciSendString("set CDAudio door open", NULL, 0, NULL);   // otvor dvierka
						mciSendString("close CDAudio", NULL, 0, NULL);
					}
													   		 	 
					LONGER_PAUSE();
					CLEAR_SCREEN();
					SHORT_PAUSE();	
				}										
				
				check_winner(player_1, player_2, setup);
				break;
				
			case 5:
				for(i = 0; i < 5; i++){
					if(player_2.army[i].health_pool != 0) //dolezite na neozivovanie jednotiek
						player_2.army[i].health_pool += 1; 
				}
				
				player_2.souls -= 2;
				setup.all_stats.souls_spent_p2 += 2;
				setup.all_stats.items_purchased_p2++;	
				
				CLEAR_SCREEN();	
				shamanism();
				system("COLOR 0F");
				
				SHORT_PAUSE();	
				
				gotoxy(25, 14);	
				printf("%s", (setup.language == 0) ? "Vsetky tvoje zive jednotky ziskali bonus 1 zivot!" : 
							  			       	 	 "All your alive units obtained bonus 1 life!");
														   
				LONGER_PAUSE();
				CLEAR_SCREEN();
				SHORT_PAUSE();											   			  			       
				break;
				
			case 6:
				for(i = 0; i < 5; i++){
					if(player_2.army[i].health_pool != 0)
						player_2.army[i].attack += 5; 
				}
				
				player_2.souls -= 3;
				setup.all_stats.souls_spent_p2 += 3;
				setup.all_stats.items_purchased_p2++;
				player_2.achievments.heart_warming++;	
				
				CLEAR_SCREEN();	
				war_heart();
				system("COLOR 0F");	
				
				SHORT_PAUSE();
				
				gotoxy(25, 14);	
				printf("%s", (setup.language == 0) ? "Vsetky tvoje zive jednotky ziskali +5 utok!" : 
											   		 "All your alive units obtained +5 attack!");
											   		 
				LONGER_PAUSE();
				CLEAR_SCREEN();
				SHORT_PAUSE();							   		 
				break;
				
			case 7:
				for(i = 0; i < 5; i++){
					if(player_2.army[i].health_pool != 0)
						player_2.army[i].defence += 5; 
				}
				
				player_2.souls -= 3;
				setup.all_stats.souls_spent_p2 += 3;
				setup.all_stats.items_purchased_p2++;
				player_2.achievments.heart_warming++;	
				
				CLEAR_SCREEN();	
				heart_of_courage();
				system("COLOR 0F");	
				
				SHORT_PAUSE();
				
				gotoxy(25, 14);	
				printf("%s", (setup.language == 0) ? "Vsetky tvoje zive jednotky ziskali +5 obranu!" : 
											   		 "All your alive units obtained +5 defence!");
											   		 
				LONGER_PAUSE();
				CLEAR_SCREEN();
				SHORT_PAUSE();							   		 
				break;
				
			case 8:
				printf("%s\n\n", (setup.language == 0) ? "Vyber si bojovnika, ktory ziska +10 utok a bonus 8 zivotov" : 
												    	 "Choose a unit that will obtain +10 attack and bonus 8 lives!");
				
				show_player_2_units_selection(player_2, setup);
				
				input = _getch();
				
				while(input - ASCII_NUM_VAL < 0 || input - ASCII_NUM_VAL > 4 || player_2.army[input - ASCII_NUM_VAL].health_pool == 0){
					ERR_MSG_OR_DEAD;
					input = _getch();
				}
				
				player_2.army[input - ASCII_NUM_VAL].health_pool += 8;
				player_2.army[input - ASCII_NUM_VAL].attack += 10;
				player_2.souls -= 4;
				setup.all_stats.souls_spent_p2 += 4;
				setup.all_stats.items_purchased_p2++;
				player_2.achievments.legendary_possession++;
				
				CLEAR_SCREEN();	
				willbreaker();
				system("COLOR 0F");
				
				SHORT_PAUSE();	
				
				gotoxy(36, 14);	
				printf("%s%s", player_2.army[input - ASCII_NUM_VAL].name, (setup.language == 0) ? " ziskal Willbreaker!" : 
																								  " obtained Willbreaker!");
																								  
				LONGER_PAUSE();
				CLEAR_SCREEN();
				SHORT_PAUSE();																				  
				break;
				
			case 9:
				printf("%s\n\n", (setup.language == 0) ? "Vyber si bojovnika, ktory ziska +10 obranu a bonus 8 zivotov!" : 
												   		 "Choose a unit that will obtain +10 defence and bonus 8 lives!");
				
				show_player_2_units_selection(player_2, setup);
				
				input = _getch();
				
				while(input - ASCII_NUM_VAL < 0 || input - ASCII_NUM_VAL > 4 || player_2.army[input - ASCII_NUM_VAL].health_pool == 0){
					ERR_MSG_OR_DEAD;
					input = _getch();
				}
				
				player_2.army[input - ASCII_NUM_VAL].health_pool += 8;
				player_2.army[input - ASCII_NUM_VAL].defence += 10;
				player_2.souls -= 4;
				setup.all_stats.souls_spent_p2 += 4;
				setup.all_stats.items_purchased_p2++;
				player_2.achievments.legendary_possession++;
				
				CLEAR_SCREEN();	
				titanskin();
				system("COLOR 0F");	
				
				SHORT_PAUSE();
				
				gotoxy(35, 14);	
				printf("%s%s", player_2.army[input - ASCII_NUM_VAL].name, (setup.language == 0) ? " ziskal Titanskin!" : 
																								  " obtained Titanskin!");
																								  
				LONGER_PAUSE();
				CLEAR_SCREEN();
				SHORT_PAUSE();																				  
				break;
				
			default:
				break;
		}					 							   						   
	}	
	else if(input == 'n') // nic or nothing
		play(player_1, player_2, setup); // jedina moznost ako sa dostat prec z menu pre magicke schopnosti, inac rekurzia (warning vo Visualku)
	
	player_2_special_abilities(player_1, player_2, setup); //rekurzia dokial vyslovene nepoviem poviem ze NECHCEM nic
}