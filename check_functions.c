/*
check_functions.c

Bojovnici 1.1 BETA
==================

Peter Cizmar 8.4.2016
*/

#include "ultimate_header.h"

extern void check_winner(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup){
	int dead_units[] = {0, 0}; //hrac 1 a 2
	register int i;
	
	CLEAR_SCREEN();
	
	for(i = 0; i < 5; i++){
		if(player_1.army[i].health_pool == 0)
			dead_units[0]++;
			
		if(player_2.army[i].health_pool == 0)
			dead_units[1]++;
	}
	
	if(dead_units[0] == 5 || dead_units[1] == 5){
		system("COLOR 0C");
		
		outro_text(setup);
		LONGER_PAUSE();
		CLEAR_SCREEN();
		SHORT_PAUSE();
		CLEAR_SCREEN();
		
		system("COLOR 0F");	
		
		if(dead_units[0] == 5){
			gotoxy(42,14);
			printf("%s%s", player_2.name,  (setup.language == 0) ? " vyhral" : " won");
			strcpy(setup.all_stats.winner, player_2.name);
			
			LONGER_PAUSE();
				
			if(dead_units[1] == 4)
				player_2.achievments.last_man_standing++;		
		}	
		else if(dead_units[1] == 5){
			gotoxy(42,14);
			printf("%s%s", player_1.name,  (setup.language == 0) ? " vyhral" : " won");
			strcpy(setup.all_stats.winner, player_1.name);
			
			LONGER_PAUSE();
				
			if(dead_units[0] == 4)
				player_1.achievments.last_man_standing++;		
		}	
		
		CLEAR_SCREEN();
		SHORT_PAUSE();
		CLEAR_SCREEN();	
		
		check_achievments(player_1, player_2, setup);	
	}
	else
	;
}

extern void check_achievments(PLAYER_INFO player_1, PLAYER_INFO player_2, SETUP_SETTINGS setup){
	register int i;
	char input, c; // c - pomocna premenna
	FILE *fr_p1, *fw_p1, *fr_p2, *fw_p2; 
	char name_file_p1[MAX_NAME_LENGTH + 4];
	char name_file_p2[MAX_NAME_LENGTH + 4];
	char final_name_file_p1[MAX_NAME_LENGTH + 11];
	char final_name_file_p2[MAX_NAME_LENGTH + 11];
	char loaded_data_p1[NUM_OF_ACHIEVMENTS];
	char loaded_data_p2[NUM_OF_ACHIEVMENTS];
	
	//nulovanie d�t pred pou�it�m
	for(i = 0; i < NUM_OF_ACHIEVMENTS; i++){
		loaded_data_p1[i] = 0;
		loaded_data_p2[i] = 0;
	}	
	
	strcpy(name_file_p1, player_1.name);
	strcat(name_file_p1, ".txt");
	strcpy(final_name_file_p1, "./saves/");
	strcat(final_name_file_p1, name_file_p1);
	
	strcpy(name_file_p2, player_2.name);
	strcat(name_file_p2, ".txt");
	strcpy(final_name_file_p2, "./saves/");
	strcat(final_name_file_p2, name_file_p2);
	
	if((fr_p1 = fopen(final_name_file_p1, "r")) != NULL){
		for(i = 0;i < NUM_OF_ACHIEVMENTS; i++){
			while((c = getc(fr_p1)) != '\n')
				loaded_data_p1[i] = c;
		}
		
		fclose(fr_p1);
	}
		
	fw_p1 = fopen(final_name_file_p1, "w");

	printf("%s%s\n\n", player_1.name, (setup.language == 0) ? ", toto su ocenenia zo vsetkych hier co si doteraz hral:" : 
															  ", these are all achievments that you earned till now:");
	
	printf("%s\t\t%s\t%s\n", (setup.language == 0) ? "Nazov uspechu" : "Name of achievment", (setup.language == 0) ? "Body" : "Points", 
						 (setup.language == 0) ? "Popis" : "Description");
	printf("===================================================================================================="); //100
	
	if(player_1.achievments.win_a_coin == 1 || (loaded_data_p1[0] - ASCII_NUM_VAL) == 1){
		printf("%s\t\t10\t", (setup.language == 0) ? "Vitazna minca" : "Winner coin");
		printf("%s\n", (setup.language == 0) ? "Vyhraj hod mincou" : "Win a coin toss");
		player_1.total_achievment_points += 10;
		
		if(player_1.achievments.win_a_coin == 1) // ak som ziskal v hre, vypisen ten z hry
			fprintf(fw_p1, "%i\n", player_1.achievments.win_a_coin);
		else if((loaded_data_p1[0] - ASCII_NUM_VAL) == 1) // ak som ho uz mal pred tym, vypis zo suboru (pola)
			fprintf(fw_p1, "%i\n", (loaded_data_p1[0] - ASCII_NUM_VAL));
		// ak oboje, tak je to jedno, lebo to bude to iste - ze ziskal		
	}
	else
		fprintf(fw_p1, "0\n");
	
	
	if(player_1.achievments.demon_summoner == 1 || (loaded_data_p1[1] - ASCII_NUM_VAL) == 1){
		printf("%s\t\t10\t", (setup.language == 0) ? "Vyvolavac" : "The Summoner");
		printf("%s\n", (setup.language == 0) ? "Vyuzi moznost vyvolat demona" : "Take a chance of summoning demon");
		player_1.total_achievment_points += 10;
		
		if(player_1.achievments.demon_summoner == 1)
			fprintf(fw_p1, "%i\n", player_1.achievments.demon_summoner);
		else if((loaded_data_p1[1] - ASCII_NUM_VAL) == 1)
			fprintf(fw_p1, "%i\n", (loaded_data_p1[1] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p1, "0\n");
	
	if(player_1.achievments.killing_spree[0] > 1 || (loaded_data_p1[2] - ASCII_NUM_VAL) > 1){
		if(player_1.achievments.killing_spree[0] > (loaded_data_p1[2] - ASCII_NUM_VAL)){
			player_1.total_achievment_points += (20 * player_1.achievments.killing_spree[0]);
			
			if(player_1.achievments.killing_spree[0] == 2)
				printf("Double kill\t\t40\t");
			else if(player_1.achievments.killing_spree[0] == 3)
				printf("Tripple kill\t\t60\t");
			else if(player_1.achievments.killing_spree[0] == 4)
				printf("Ultra kill\t\t80\t");
			else if(player_1.achievments.killing_spree[0] == 5)
				printf("Rampage\t\t\t100\t");
		}
		else if(player_1.achievments.killing_spree[0] < (loaded_data_p1[2] - ASCII_NUM_VAL)){
			player_1.total_achievment_points += (20 * (loaded_data_p1[2] - ASCII_NUM_VAL));
			
			if((loaded_data_p1[2] - ASCII_NUM_VAL) == 2)
				printf("Double kill\t\t40\t");
			else if((loaded_data_p1[2] - ASCII_NUM_VAL) == 3)
				printf("Tripple kill\t\t60\t");
			else if((loaded_data_p1[2] - ASCII_NUM_VAL) == 4)
				printf("Ultra kill\t\t80\t");
			else if((loaded_data_p1[2] - ASCII_NUM_VAL) == 5)
				printf("Rampage\t\t\t100\t");
		}
		printf("%s\n", (setup.language == 0) ? "Double kill - 2 zabitia, Tripple kill - 3, ..." : 
											   "Double kill - 2 kills, Tripple kill - 3, ...");
											   
		if(player_1.achievments.killing_spree[0] > 1 && player_1.achievments.killing_spree[0] > (loaded_data_p1[2] - ASCII_NUM_VAL))
			fprintf(fw_p1, "%i\n", player_1.achievments.killing_spree[0]);
		else if(loaded_data_p1[2] > 1 && player_1.achievments.killing_spree[0] < (loaded_data_p1[2] - ASCII_NUM_VAL))
			fprintf(fw_p1, "%i\n", (loaded_data_p1[2] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p1, "0\n");
	
	if(player_1.achievments.alchemist >= 4 || (loaded_data_p1[3] - ASCII_NUM_VAL) >= 4){
		printf("%s\t\t20\t", (setup.language == 0) ? "Alchemista" : "Alchemist");
		printf("%s\n", (setup.language == 0) ? "Vytvor dokopy 4 a viac elixirov" : "Make 4 or more potions");
		player_1.total_achievment_points += 20;
		
		if(player_1.achievments.alchemist >= 4)
			fprintf(fw_p1, "%i\n", player_1.achievments.alchemist);
		else if((loaded_data_p1[3] - ASCII_NUM_VAL) >= 4)
			fprintf(fw_p1, "%i\n", (loaded_data_p1[3] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p1, "0\n");
	
	if(player_1.achievments.soldier >= 4 || (loaded_data_p1[4] - ASCII_NUM_VAL) >= 4){
		printf("%s\t\t\t20\t", (setup.language == 0) ? "Vojak" : "Soldier");
		printf("%s\n", (setup.language == 0) ? "Vystrel aspon 4x z magickej pusky" : "Shoot at least 4 times from the magic rifle");
		player_1.total_achievment_points += 20;
		
		if(player_1.achievments.soldier >= 4)
			fprintf(fw_p1, "%i\n", player_1.achievments.soldier);
		else if((loaded_data_p1[4] - ASCII_NUM_VAL) >= 4)
			fprintf(fw_p1, "%i\n", (loaded_data_p1[4] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p1, "0\n");
	
	if(player_1.achievments.sniper >= 2 || (loaded_data_p1[5] - ASCII_NUM_VAL) >= 2){
		printf("%s\t\t\t20\t", (setup.language == 0) ? "Snajper" : "Sniper");
		printf("%s\n", (setup.language == 0) ? "Doraz aspon 2 nepriatelskych bojovnikov s magickou puskou" : "Finish at least 2 enemy units with magic rifle");
		player_1.total_achievment_points += 20;
		
		if(player_1.achievments.sniper >= 2)
			fprintf(fw_p1, "%i\n", player_1.achievments.sniper);
		else if((loaded_data_p1[5] - ASCII_NUM_VAL) >= 2)
			fprintf(fw_p1, "%i\n", (loaded_data_p1[5] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p1, "0\n");
	
	if(player_1.achievments.mass_destruction >= 2 || (loaded_data_p1[6] - ASCII_NUM_VAL) >= 2){
		printf("%s\t20\t", (setup.language == 0) ? "Masova destrukcia" : "Mass destruction");
		printf("%s\n", (setup.language == 0) ? "Doraz aspon 2 nepriatelskych bojovnikov s dazdom dusi" : "Finish at least 2 enemy soldiers with storm of souls");
		player_1.total_achievment_points += 20;
		
		if(player_1.achievments.mass_destruction >= 2)
			fprintf(fw_p1, "%i\n", player_1.achievments.mass_destruction);
		else if((loaded_data_p1[6] - ASCII_NUM_VAL) >= 2)
			fprintf(fw_p1, "%i\n", (loaded_data_p1[6] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p1, "0\n");
	
	if(player_1.achievments.legendary_possession >= 1  || (loaded_data_p1[7] - ASCII_NUM_VAL) >= 1){
		printf("%s\t\t30\t", (setup.language == 0) ? "Legendarna moc" : "Legendary power");
		printf("%s\n", (setup.language == 0) ? "Ziskaj jednu z legendarnych zbrani (Willbreaker / Titanskin)" : "Obtain of the legendary weapons (Willbreaker / Titanskin)");
		player_1.total_achievment_points += 30;
		
		if(player_1.achievments.legendary_possession >= 1)
			fprintf(fw_p1, "%i\n", player_1.achievments.legendary_possession);
		else if((loaded_data_p1[7] - ASCII_NUM_VAL) >= 1)
			fprintf(fw_p1, "%i\n", (loaded_data_p1[7] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p1, "0\n");
	
	if(player_1.achievments.heart_warming >= 1 || (loaded_data_p1[8] - ASCII_NUM_VAL) >= 1){
		printf("%s\t\t20\t", (setup.language == 0) ? "Dobre srdce" : "Heart warming");
		printf("%s\n", (setup.language == 0) ? "Ziskaj jedno zo srdcii" : "Obtain 1 heart");
		player_1.total_achievment_points += 20;
		
		if(player_1.achievments.heart_warming >= 1)
			fprintf(fw_p1, "%i\n", player_1.achievments.heart_warming);
		else if((loaded_data_p1[8] - ASCII_NUM_VAL) >= 1)
			fprintf(fw_p1, "%i\n", (loaded_data_p1[8] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p1, "0\n");
		
	if(player_1.achievments.last_man_standing == 1 || (loaded_data_p1[9] - ASCII_NUM_VAL) == 1){
		printf("%s\t\t10\t", (setup.language == 0) ? "Posledny muz" : "Last man");
		printf("%s\n", (setup.language == 0) ? "Vyhraj tak, ze ti ostane iba jeden bojovnik" : "Win in the way that you will only have 1 last unit left");
		player_1.total_achievment_points += 10;
		
		if(player_1.achievments.last_man_standing == 1)
			fprintf(fw_p1, "%i\n", player_1.achievments.last_man_standing);
		else if((loaded_data_p1[9] - ASCII_NUM_VAL) == 1)
			fprintf(fw_p1, "%i\n", (loaded_data_p1[9] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p1, "0\n");
	
	if(player_1.achievments.brothers_in_war == 1 || (loaded_data_p1[10] - ASCII_NUM_VAL) == 1){
		printf("%s\t\t10\t", (setup.language == 0) ? "Bratia vo vojne" : "Brothers in war");
		printf("%s\n", (setup.language == 0) ? "Zachran svojho utocnika tym ze ho vymenis pred bojom" : "Safe your attacking unit by swapping him before the fight");
		player_1.total_achievment_points += 10;
		
		if(player_1.achievments.brothers_in_war == 1)
			fprintf(fw_p1, "%i\n", player_1.achievments.brothers_in_war);
		else if((loaded_data_p1[10] - ASCII_NUM_VAL) == 1)
			fprintf(fw_p1, "%i\n", (loaded_data_p1[10] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p1, "0\n");	
	
	if(player_1.achievments.lucky_strike[0] == 1 || (loaded_data_p1[11] - ASCII_NUM_VAL) == 1){
		printf("%s\t\t30\t", (setup.language == 0) ? "Stastny uder" : "Lucky strike");
		printf("%s\n", (setup.language == 0) ? "Zasiahni nepriatela 5x po sebe" : "Hit the enemy 5 times in a row");
		player_1.total_achievment_points += 30;
		
		if(player_1.achievments.lucky_strike[0] == 1)
			fprintf(fw_p1, "%i\n", player_1.achievments.lucky_strike[0]);
		else if((loaded_data_p1[11] - ASCII_NUM_VAL) == 1)
			fprintf(fw_p1, "%i\n", (loaded_data_p1[11] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p1, "0\n");
	
	if(player_1.achievments.bad_luck_brian[0] == 1 || (loaded_data_p1[12] - ASCII_NUM_VAL) == 1){
		printf("Bad luck Brian\t\t30\t");
		printf("%s\n", (setup.language == 0) ? "Netraf nepriatela 5x po sebe" : "Do not hit the enemy 5 times in a row");
		player_1.total_achievment_points += 30;
		
		if(player_1.achievments.bad_luck_brian[0] == 1)
			fprintf(fw_p1, "%i\n", player_1.achievments.bad_luck_brian[0]);
		else if((loaded_data_p1[12] - ASCII_NUM_VAL) == 1)
			fprintf(fw_p1, "%i\n", (loaded_data_p1[12] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p1, "0\n");
	
	if(player_1.achievments.easter_egg == 1 || (loaded_data_p1[13] - ASCII_NUM_VAL) == 1){
		printf("?\t\t\t10\t");
		printf("%s\n", (setup.language == 0) ? "Mas to spravne meno!" : "You have to right name!");
		player_1.total_achievment_points += 10;
		
		if(player_1.achievments.easter_egg == 1)
			fprintf(fw_p1, "%i\n", player_1.achievments.easter_egg);
		else if((loaded_data_p1[13] - ASCII_NUM_VAL) == 1)
			fprintf(fw_p1, "%i\n", (loaded_data_p1[13] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p1, "0\n");
	
	if((player_1.achievments.no_bonus_baby == 1 && strcmp(player_1.name, setup.all_stats.winner) == 0) || ((loaded_data_p1[14] - ASCII_NUM_VAL) == 1)){
		printf("No bonus baby\t\t50\t");
		printf("%s\n", (setup.language == 0) ? "Vyhraj bez kliknutia na bonusy (minca je dovolena)" : "Win with out clicking the bonuses (coin is allowed)");
		player_1.total_achievment_points += 50;
		
		if((player_1.achievments.no_bonus_baby == 1) && (strcmp(player_1.name, setup.all_stats.winner) == 0))
			fprintf(fw_p1, "%i\n", player_1.achievments.no_bonus_baby);
		else if((loaded_data_p1[14] - ASCII_NUM_VAL) == 1)
			fprintf(fw_p1, "%i\n", (loaded_data_p1[14] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p1, "0\n");
	
	printf("===================================================================================================="); //100
	printf("TOTAL\t\t\t%i", player_1.total_achievment_points);
	
	fclose(fw_p1);
	
	printf("\n\n\n%s", (setup.language == 0) ? "Stlac lubovolnu klavesu pre zobrazenie uspechov druheho hraca!" : "Press any key to show achievments of player 2!");
	input = _getch();
	CLEAR_SCREEN();	
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	if ((fr_p2 = fopen(final_name_file_p2, "r")) != NULL) {
		for (i = 0; i < NUM_OF_ACHIEVMENTS; i++) {
			while ((c = getc(fr_p2)) != '\n')
				loaded_data_p2[i] = c;
		}

		fclose(fr_p2);
	}

	fw_p2 = fopen(final_name_file_p2, "w");
	
	printf("%s%s\n\n", player_2.name, (setup.language == 0) ? ", toto su ocenenia zo vsetkych hier co si doteraz hral:" : 
															  ", these are all achievments that you earned till now:");
	
	printf("%s\t\t%s\t%s\n", (setup.language == 0) ? "Nazov uspechu" : "Name of achievment", (setup.language == 0) ? "Body" : "Points", 
						 (setup.language == 0) ? "Popis" : "Description");
	printf("===================================================================================================="); //100
	
	if(player_2.achievments.win_a_coin == 1 || (loaded_data_p2[0] - ASCII_NUM_VAL) == 1){
		printf("%s\t\t10\t", (setup.language == 0) ? "Vitazna minca" : "Winner coin");
		printf("%s\n", (setup.language == 0) ? "Vyhraj hod mincou" : "Win a coin toss");
		player_2.total_achievment_points += 10;
		
		if(player_2.achievments.win_a_coin == 1) // ak som ziskal v hre, vypisen ten z hry
			fprintf(fw_p2, "%i\n", player_2.achievments.win_a_coin);
		else if((loaded_data_p2[0] - ASCII_NUM_VAL) == 1) // ak som ho uz mal pred tym, vypis zo suboru (pola)
			fprintf(fw_p2, "%i\n", (loaded_data_p2[0] - ASCII_NUM_VAL));
		// ak oboje, tak je to jedno, lebo to bude to iste - ze ziskal		
	}
	else
		fprintf(fw_p2, "0\n");
	
	
	if(player_2.achievments.demon_summoner == 1 || (loaded_data_p2[1] - ASCII_NUM_VAL) == 1){
		printf("%s\t\t10\t", (setup.language == 0) ? "Vyvolavac" : "The Summoner");
		printf("%s\n", (setup.language == 0) ? "Vyuzi moznost vyvolat demona" : "Take a chance of summoning demon");
		player_2.total_achievment_points += 10;
		
		if(player_2.achievments.demon_summoner == 1)
			fprintf(fw_p2, "%i\n", player_2.achievments.demon_summoner);
		else if((loaded_data_p2[1] - ASCII_NUM_VAL) == 1)
			fprintf(fw_p2, "%i\n", (loaded_data_p2[1] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p2, "0\n");
	
	if(player_2.achievments.killing_spree[0] > 1 || (loaded_data_p2[2] - ASCII_NUM_VAL) > 1){
		if(player_2.achievments.killing_spree[0] > (loaded_data_p2[2] - ASCII_NUM_VAL)){
			player_2.total_achievment_points += (20 * player_2.achievments.killing_spree[0]);
			
			if(player_2.achievments.killing_spree[0] == 2)
				printf("Double kill\t\t40\t");
			else if(player_2.achievments.killing_spree[0] == 3)
				printf("Tripple kill\t\t60\t");
			else if(player_2.achievments.killing_spree[0] == 4)
				printf("Ultra kill\t\t80\t");
			else if(player_2.achievments.killing_spree[0] == 5)
				printf("Rampage\t\t\t100\t");
		}
		else if(player_2.achievments.killing_spree[0] < (loaded_data_p2[2] - ASCII_NUM_VAL)){
			player_2.total_achievment_points += (20 * (loaded_data_p2[2] - ASCII_NUM_VAL));
			
			if((loaded_data_p2[2] - ASCII_NUM_VAL) == 2)
				printf("Double kill\t\t40\t");
			else if((loaded_data_p2[2] - ASCII_NUM_VAL) == 3)
				printf("Tripple kill\t\t60\t");
			else if((loaded_data_p2[2] - ASCII_NUM_VAL) == 4)
				printf("Ultra kill\t\t80\t");
			else if((loaded_data_p2[2] - ASCII_NUM_VAL) == 5)
				printf("Rampage\t\t\t100\t");
		}
		printf("%s\n", (setup.language == 0) ? "Double kill - 2 zabitia, Tripple kill - 3, ..." : 
											   "Double kill - 2 kills, Tripple kill - 3, ...");
											   
		if(player_2.achievments.killing_spree[0] > 1 && player_2.achievments.killing_spree[0] > (loaded_data_p2[2] - ASCII_NUM_VAL))
			fprintf(fw_p2, "%i\n", player_2.achievments.killing_spree[0]);
		else if(loaded_data_p2[2] > 1 && player_2.achievments.killing_spree[0] < (loaded_data_p2[2] - ASCII_NUM_VAL))
			fprintf(fw_p2, "%i\n", (loaded_data_p2[2] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p2, "0\n");
	
	if(player_2.achievments.alchemist >= 4 || (loaded_data_p2[3] - ASCII_NUM_VAL) >= 4){
		printf("%s\t\t20\t", (setup.language == 0) ? "Alchemista" : "Alchemist");
		printf("%s\n", (setup.language == 0) ? "Vytvor dokopy 4 a viac elixirov" : "Make 4 or more potions");
		player_2.total_achievment_points += 20;
		
		if(player_2.achievments.alchemist >= 4)
			fprintf(fw_p2, "%i\n", player_2.achievments.alchemist);
		else if((loaded_data_p2[3] - ASCII_NUM_VAL) >= 4)
			fprintf(fw_p2, "%i\n", (loaded_data_p2[3] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p2, "0\n");
	
	if(player_2.achievments.soldier >= 4 || (loaded_data_p2[4] - ASCII_NUM_VAL) >= 4){
		printf("%s\t\t\t20\t", (setup.language == 0) ? "Vojak" : "Soldier");
		printf("%s\n", (setup.language == 0) ? "Vystrel aspon 4x z magickej pusky" : "Shoot at least 4 times from the magic rifle");
		player_2.total_achievment_points += 20;
		
		if(player_2.achievments.soldier >= 4)
			fprintf(fw_p2, "%i\n", player_2.achievments.soldier);
		else if((loaded_data_p2[4] - ASCII_NUM_VAL) >= 4)
			fprintf(fw_p2, "%i\n", (loaded_data_p2[4] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p2, "0\n");
	
	if(player_2.achievments.sniper >= 2 || (loaded_data_p2[5] - ASCII_NUM_VAL) >= 2){
		printf("%s\t\t\t20\t", (setup.language == 0) ? "Snajper" : "Sniper");
		printf("%s\n", (setup.language == 0) ? "Doraz aspon 2 nepriatelskych bojovnikov s magickou puskou" : "Finish at least 2 enemy units with magic rifle");
		player_2.total_achievment_points += 20;
		
		if(player_2.achievments.sniper >= 2)
			fprintf(fw_p2, "%i\n", player_2.achievments.sniper);
		else if((loaded_data_p2[5] - ASCII_NUM_VAL) >= 2)
			fprintf(fw_p2, "%i\n", (loaded_data_p2[5] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p2, "0\n");
	
	if(player_2.achievments.mass_destruction >= 2 || (loaded_data_p2[6] - ASCII_NUM_VAL) >= 2){
		printf("%s\t20\t", (setup.language == 0) ? "Masova destrukcia" : "Mass destruction");
		printf("%s\n", (setup.language == 0) ? "Doraz aspon 2 nepriatelskych bojovnikov s dazdom dusi" : "Finish at least 2 enemy soldiers with storm of souls");
		player_2.total_achievment_points += 20;
		
		if(player_2.achievments.mass_destruction >= 2)
			fprintf(fw_p2, "%i\n", player_2.achievments.mass_destruction);
		else if((loaded_data_p2[6] - ASCII_NUM_VAL) >= 2)
			fprintf(fw_p2, "%i\n", (loaded_data_p2[6] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p2, "0\n");
	
	if(player_2.achievments.legendary_possession >= 1  || (loaded_data_p2[7] - ASCII_NUM_VAL) >= 1){
		printf("%s\t\t30\t", (setup.language == 0) ? "Legendarna moc" : "Legendary power");
		printf("%s\n", (setup.language == 0) ? "Ziskaj jednu z legendarnych zbrani (Willbreaker / Titanskin)" : "Obtain of the legendary weapons (Willbreaker / Titanskin)");
		player_2.total_achievment_points += 30;
		
		if(player_2.achievments.legendary_possession >= 1)
			fprintf(fw_p2, "%i\n", player_2.achievments.legendary_possession);
		else if((loaded_data_p2[7] - ASCII_NUM_VAL) >= 1)
			fprintf(fw_p2, "%i\n", (loaded_data_p2[7] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p2, "0\n");
	
	if(player_2.achievments.heart_warming >= 1 || (loaded_data_p2[8] - ASCII_NUM_VAL) >= 1){
		printf("%s\t\t20\t", (setup.language == 0) ? "Dobre srdce" : "Heart warming");
		printf("%s\n", (setup.language == 0) ? "Ziskaj jedno zo srdcii" : "Obtain 1 heart");
		player_2.total_achievment_points += 20;
		
		if(player_2.achievments.heart_warming >= 1)
			fprintf(fw_p2, "%i\n", player_2.achievments.heart_warming);
		else if((loaded_data_p2[8] - ASCII_NUM_VAL) >= 1)
			fprintf(fw_p2, "%i\n", (loaded_data_p2[8] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p2, "0\n");
		
	if(player_2.achievments.last_man_standing == 1 || (loaded_data_p2[9] - ASCII_NUM_VAL) == 1){
		printf("%s\t\t10\t", (setup.language == 0) ? "Posledny muz" : "Last man");
		printf("%s\n", (setup.language == 0) ? "Vyhraj tak, ze ti ostane iba jeden bojovnik" : "Win in the way that you will only have 1 last unit left");
		player_2.total_achievment_points += 10;
		
		if(player_2.achievments.last_man_standing == 1)
			fprintf(fw_p2, "%i\n", player_2.achievments.last_man_standing);
		else if((loaded_data_p2[9] - ASCII_NUM_VAL) == 1)
			fprintf(fw_p2, "%i\n", (loaded_data_p2[9] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p2, "0\n");
	
	if(player_2.achievments.brothers_in_war == 1 || (loaded_data_p2[10] - ASCII_NUM_VAL) == 1){
		printf("%s\t\t10\t", (setup.language == 0) ? "Bratia vo vojne" : "Brothers in war");
		printf("%s\n", (setup.language == 0) ? "Zachran svojho utocnika tym ze ho vymenis pred bojom" : "Safe your attacking unit by swapping him before the fight");
		player_2.total_achievment_points += 10;
		
		if(player_2.achievments.brothers_in_war == 1)
			fprintf(fw_p2, "%i\n", player_2.achievments.brothers_in_war);
		else if((loaded_data_p2[10] - ASCII_NUM_VAL) == 1)
			fprintf(fw_p2, "%i\n", (loaded_data_p2[10] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p2, "0\n");	
	
	if(player_2.achievments.lucky_strike[0] == 1 || (loaded_data_p2[11] - ASCII_NUM_VAL) == 1){
		printf("%s\t\t30\t", (setup.language == 0) ? "Stastny uder" : "Lucky strike");
		printf("%s\n", (setup.language == 0) ? "Zasiahni nepriatela 5x po sebe" : "Hit the enemy 5 times in a row");
		player_2.total_achievment_points += 30;
		
		if(player_2.achievments.lucky_strike[0] == 1)
			fprintf(fw_p2, "%i\n", player_2.achievments.lucky_strike[0]);
		else if((loaded_data_p2[11] - ASCII_NUM_VAL) == 1)
			fprintf(fw_p2, "%i\n", (loaded_data_p2[11] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p2, "0\n");
	
	if(player_2.achievments.bad_luck_brian[0] == 1 || (loaded_data_p2[12] - ASCII_NUM_VAL) == 1){
		printf("Bad luck Brian\t\t30\t");
		printf("%s\n", (setup.language == 0) ? "Netraf nepriatela 5x po sebe" : "Do not hit the enemy 5 times in a row");
		player_2.total_achievment_points += 30;
		
		if(player_2.achievments.bad_luck_brian[0] == 1)
			fprintf(fw_p2, "%i\n", player_2.achievments.bad_luck_brian[0]);
		else if((loaded_data_p2[12] - ASCII_NUM_VAL) == 1)
			fprintf(fw_p2, "%i\n", (loaded_data_p2[12] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p2, "0\n");
	
	if(player_2.achievments.easter_egg == 1 || (loaded_data_p2[13] - ASCII_NUM_VAL) == 1){
		printf("?\t\t\t10\t");
		printf("%s\n", (setup.language == 0) ? "Mas to spravne meno!" : "You have to right name!");
		player_2.total_achievment_points += 10;
		
		if(player_2.achievments.easter_egg == 1)
			fprintf(fw_p2, "%i\n", player_2.achievments.easter_egg);
		else if((loaded_data_p2[13] - ASCII_NUM_VAL) == 1)
			fprintf(fw_p2, "%i\n", (loaded_data_p2[13] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p2, "0\n");
	
	if((player_2.achievments.no_bonus_baby == 1 && strcmp(player_2.name, setup.all_stats.winner) == 0) || ((loaded_data_p2[14] - ASCII_NUM_VAL) == 1)){
		printf("No bonus baby\t\t50\t");
		printf("%s\n", (setup.language == 0) ? "Vyhraj bez kliknutia na bonusy (minca je dovolena)" : "Win with out clicking the bonuses (coin is allowed)");
		player_2.total_achievment_points += 50;
		
		if(player_2.achievments.no_bonus_baby == 1 && strcmp(player_2.name, setup.all_stats.winner))
			fprintf(fw_p2, "%i\n", player_2.achievments.no_bonus_baby);
		else if((loaded_data_p2[14] - ASCII_NUM_VAL) == 1)
			fprintf(fw_p2, "%i\n", (loaded_data_p2[14] - ASCII_NUM_VAL));
	}
	else
		fprintf(fw_p2, "0\n");
	
	printf("===================================================================================================="); //100
	printf("TOTAL\t\t\t%i", player_2.total_achievment_points);
	
	fclose(fw_p2);
	
	printf("\n\n\n%s", (setup.language == 0) ? "Stlac lubovolnu klavesu pre ulozenie dat z hry!" : "Press any key to save data from the game!");
	input = _getch();
	CLEAR_SCREEN();	
	
	save_data(player_1, player_2, setup);
}