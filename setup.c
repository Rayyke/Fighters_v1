/*
setup.c

Bojovnici 1.1 BETA
==================

Peter Cizmar 8.4.2016
*/

#include "ultimate_header.h"

extern void gotoxy(int x,int y){
	COORD coord = {0,0};

	coord.X = x; 
	coord.Y = y; 
	
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),coord);	
}

extern void adjust_window_size(void){
	// kod funkcie pouzity z: http://www.programming-techniques.com/2011/09/how-to-resize-console-window-using-c.html

	HANDLE wHnd;
	HANDLE rHnd;

    wHnd = GetStdHandle(STD_OUTPUT_HANDLE);
    rHnd = GetStdHandle(STD_INPUT_HANDLE);
    
    SMALL_RECT windowSize = {0, 0, 100, 30};
    SetConsoleWindowInfo(wHnd, 1, &windowSize);
    
    COORD bufferSize = {100, 30};
    SetConsoleScreenBufferSize(wHnd, bufferSize);  
}

extern void hide_cursor(void){
	HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO info;
	info.dwSize = 100;
	info.bVisible = FALSE;
	SetConsoleCursorInfo(consoleHandle, &info);
}

extern void basic_setup(SETUP_SETTINGS setup){
	PLAYER_INFO player_1;
	PLAYER_INFO player_2;

	// basic setup
	setup.first_round 			= 0;
	setup.language 				= setup.language; // nic nerobi, jazyk sa voli fixne pri spusteni
	setup.who_attacks 			= 1;
	setup.abilities 			= 1;
	setup.abilities_allow[0] 	= 0;
	setup.abilities_allow[1] 	= 0;
	setup.active_unit[0] 		= 0;
	setup.active_unit[1] 		= 0;
	setup.timer_game[0]			= 0;
	setup.timer_game[1]			= 0;
	setup.timer_fight[0]		= 0;
	setup.timer_fight[1]		= 0;
	setup.music_allowed			= 1;
	setup.cd_rom_eject_allowed	= 1;
	setup.presentation_mode		= 0;
	
	//basic setup pre hraca 1
	player_1.souls 						= 0; 
	player_1.demon_available 			= 1; 
	player_1.swap_available 			= 1; 
	player_1.total_achievment_points 	= 0;
	player_1.alive_units				= (setup.presentation_mode == 1) ? 3 : 5;
		
	//basic setup pre hraca 2
	player_2.souls 						= 0;	
	player_2.demon_available 			= 1;
	player_2.swap_available 			= 1;
	player_2.total_achievment_points 	= 0;
	player_2.alive_units				= (setup.presentation_mode == 1) ? 3 : 5;
	
	// basic achievment setup pre hraca 1
	player_1.achievments.win_a_coin 			= 0;
	player_1.achievments.demon_summoner			= 0;
	player_1.achievments.killing_spree[0] 		= 0;
	player_1.achievments.killing_spree[1] 		= 0;
	player_1.achievments.alchemist 				= 0;
	player_1.achievments.soldier 				= 0;
	player_1.achievments.sniper 				= 0;
	player_1.achievments.mass_destruction 		= 0;
	player_1.achievments.legendary_possession 	= 0;
	player_1.achievments.last_man_standing 		= 0;
	player_1.achievments.brothers_in_war 		= 0;
	player_1.achievments.lucky_strike[0] 		= 0;
	player_1.achievments.lucky_strike[1] 		= 0;
	player_1.achievments.bad_luck_brian[0] 		= 0;
	player_1.achievments.bad_luck_brian[1] 		= 0;
	player_1.achievments.easter_egg 			= 0;
	player_1.achievments.no_bonus_baby			= 1;
	player_1.achievments.heart_warming			= 0;
	
	// basic achievment setup pre hraca 2
	player_2.achievments.win_a_coin 			= 0;
	player_2.achievments.demon_summoner			= 0;
	player_2.achievments.killing_spree[0] 		= 0;
	player_2.achievments.killing_spree[1] 		= 0;
	player_2.achievments.alchemist 				= 0;
	player_2.achievments.soldier 				= 0;
	player_2.achievments.sniper 				= 0;
	player_2.achievments.mass_destruction 		= 0;
	player_2.achievments.legendary_possession 	= 0;
	player_2.achievments.last_man_standing 		= 0;
	player_2.achievments.brothers_in_war 		= 0;
	player_2.achievments.lucky_strike[0] 		= 0;
	player_2.achievments.lucky_strike[1] 		= 0;
	player_2.achievments.bad_luck_brian[0] 		= 0;
	player_2.achievments.bad_luck_brian[1] 		= 0;
	player_2.achievments.easter_egg 			= 0;
	player_2.achievments.no_bonus_baby			= 1;
	player_2.achievments.heart_warming			= 0;
	
	// basic stats setup - overall
	strcpy(setup.all_stats.language, setup.all_stats.language); // nic nerobi, jazyk sa voli fixne pri spusteni
	strcpy(setup.all_stats.allowed_bonuses, " ");
	strcpy(setup.all_stats.winner_of_coin 	, " ");
	strcpy(setup.all_stats.winner, " ");
	strcpy(setup.all_stats.side_of_coin, " ");
	setup.all_stats.number_of_fights						= 0;
	setup.all_stats.souls_spent								= 0;
	setup.all_stats.summoned_demons							= 0;
	setup.all_stats.swap_used								= 0;
	setup.all_stats.items_purchased							= 0;	
	setup.all_stats.average_fight_duration					= 0.0;
	setup.all_stats.fight_durations_total					= 0.0;
	setup.all_stats.game_duration							= 0.0;
	setup.all_stats.average_attack_power_generated			= 0.0;
	setup.all_stats.average_defence_power_generated			= 0.0;
	setup.all_stats.average_attack_defence_rate_generated	= 0.0;
	   
	// basic stats setup -  hrac 1
	strcpy(setup.all_stats.summoned_demon_p1, (setup.language == 0) ? "NIE" : "NO");
	strcpy(setup.all_stats.swap_used_p1, (setup.language == 0) ? "NIE" : "NO");
	setup.all_stats.units_left_p1								= 0;
	setup.all_stats.souls_spent_p1								= 0;
	setup.all_stats.items_purchased_p1							= 0;
	setup.all_stats.hits_p1										= 0;
	setup.all_stats.non_hits_p1									= 0;
	setup.all_stats.hit_rate_p1									= 0.0;
	setup.all_stats.average_attack_power_p1_generated			= 0.0;
	setup.all_stats.average_defence_power_p1_generated			= 0.0;
	setup.all_stats.average_attack_defence_rate_p1_generated	= 0.0;
	   	
	// basic stats setup - hrac 2
	strcpy(setup.all_stats.summoned_demon_p2, (setup.language == 0) ? "NIE" : "NO");
	strcpy(setup.all_stats.swap_used_p2, (setup.language == 0) ? "NIE" : "NO");
	setup.all_stats.units_left_p2								= 0;
	setup.all_stats.souls_spent_p2								= 0;
	setup.all_stats.items_purchased_p2							= 0;
	setup.all_stats.hits_p2										= 0;
	setup.all_stats.non_hits_p2									= 0;
	setup.all_stats.hit_rate_p2									= 0.0;
	setup.all_stats.average_attack_power_p2_generated			= 0.0;
	setup.all_stats.average_defence_power_p2_generated			= 0.0;
	setup.all_stats.average_attack_defence_rate_p2_generated	= 0.0;
	
	load(player_1, player_2, setup);
}